<?php
ob_start();

$idUser = $_SESSION['id_user'];
if(isset($_POST['kirim'])){
    
   
    // $namaPengadu = mysqli_real_escape_string($link,$_POST['nama_pengadu']);
    // $ktp = mysqli_real_escape_string($link,$_POST['ktp']);
    $alamat = mysqli_real_escape_string($link,$_POST['alamat']);
    $telp = mysqli_real_escape_string($link,$_POST['telp']);
    $statusPengadu = mysqli_real_escape_string($link,$_POST['status_pengadu']);
    $namaKorban = mysqli_real_escape_string($link,$_POST['nama_korban']);
    $jenisKelamin = mysqli_real_escape_string($link,$_POST['jenis_kelamin']);
    $usiaKorban = mysqli_real_escape_string($link,$_POST['usia_korban']);
    // $files = mysqli_real_escape_string($link,$_POST['files']);
    $deskripsi = mysqli_real_escape_string($link,$_POST['deskripsi']);


    $targetDir = "filePhoto/";
    $filename = uniqid() . rand(1,99999) . $_FILES["files"]["name"];
    // list($width, $height, $type, $attr) = getimagesize($filename);
    $targetFilePath = $targetDir .  $filename  ;
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
    // $source = $_FILES["file"]["tmp_name"]  ;
    // print_r($filename);
    if(!empty($_FILES["files"]["name"])){
        //izinkan file yang diupload

        $allowType = array("jpg","png","jpeg","JPG");
        if(in_array($fileType,$allowType)){
            //upload to server
            // if($_FILES['fl_photo']['size'] < 10000000){
                    
                    if(move_uploaded_file($_FILES["files"]["tmp_name"], $targetFilePath)){
                        $query = mysqli_query($link,"INSERT INTO tm_pengaduan ( id_login,
                                                            alamat,
                                                            telp,
                                                            status_pengadu,
                                                            nama_korban,
                                                            jenis_kelamin,
                                                            usia_korban,
                                                            files,
                                                            deskripsi) VALUES 
                                                            ('$idUser','$alamat','$telp',
                                                            '$statusPengadu','$namaKorban','$jenisKelamin',
                                                            '$usiaKorban','$filename','$deskripsi'
                                                            )");
    
                            if($query){
                                echo "<script>alert('sukses tambah data');window.location.assign(\"page.php?page=data\")</script>";
                            }else {
                                echo "<script>alert('gagal tambah data');window.location.assign(\"page.php?page=data\")</script>";
                            }
                    }
                    else {
                        echo '<script>alert("gagal upload ke folder")</script>';
                        
                    }
            // }
            // else {
            //     echo "<script>alert('ukuran harus dibawah banget')</script>";
            // }   
        }
        else {
            echo "pastikan format jpg,png,jpeg,JPG";
        }
    }
    else {
        echo "silahkan pilih file";
    }
    
}
?>

<button  class="btn-circle btn-circle-raised btn-circle-primary" data-toggle="modal" data-target="#myModal6">
    <i class="zmdi zmdi-plus"></i>
</button>
<button  class="btn-circle btn-circle-raised btn-circle-success" onclick="location.reload()">
    <i class="zmdi zmdi-refresh"></i>
</button>
<div class="modal modal-success" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel6">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                <h3 class="modal-title" id="myModalLabel6">Form Pengaduan</h3>
            </div>
            <div class="modal-body">
            
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Nama Pelapor</label>
                        <input type="text" class="form-control" style="text-transform: uppercase;" value="<?php echo $_SESSION['namaDepan'] . ' ' . $_SESSION['namaBelakang'] ?>" required="required" readonly>
                    </div>
                    <div class="form-group">
                        <label>KTP</label><br/>
                        <input type="text" class="form-control"  maxlength="16" required="required" value="<?php echo $_SESSION['ktp'] ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Alamat Lengkap Pelapor</label>
                        <textarea class="form-control" name="alamat" cols="5" rows="5" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="number" class="form-control" name="telp" required="required">
                    </div>
                    <div class="form-group">
                        <label>Status Pelapor</label>
                        <select class="form-control selectpicker" name="status_pengadu">
                            <option value="1">-</option>
                            <option value="2">Korban</option>
                            <option value="3">Orang Tua</option>
                            <option value="4">Kakak / Adik</option>
                            <option value="5">Keluarga Dekat</option>
                            <option value="6">Tetangga</option>
                            <option value="7">Pendamping</option>
                            <option value="8">Orang Lain</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Korban</label>
                        <input type="text" class="form-control" style="text-transform: uppercase;" name="nama_korban" required="required">
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <select class="form-control selectpicker" name="jenis_kelamin">
                            <option value="1">Laki - Laki</option>
                            <option value="2">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Usia Korban</label>
                        <input type="number" class="form-control" name="usia_korban"  required="required">
                    </div>
                    <div class="form-group">
                        <label>Upload Gambar</label>
                        <input type="text" readonly=""  class="form-control" placeholder="Browse...">
                        <input type="file" id="inputFile" name="files" accept="image/*">
                        
                    </div>
                    <div class="form-group">
                        <label>Deskripsi Singkat</label>
                        <textarea class="form-control" name="deskripsi" cols="5" rows="5" required="required"></textarea>
                    </div>  
                    <div class="form-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit"  name="kirim" class="btn btn-raised btn-success">Kirim</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>

<table class="table table-responsive" style="margin-top:2%;">
    <thead>
        <tr>
            <th>Nomor</th>
            <th>Nama Korban</th>
            <th>Status Laporan</th>
        </tr>
    </thead>
    <tbody>
    <?php 
        $data = mysqli_query($link,"SELECT * FROM tm_pengaduan WHERE id_login ='$idUser'");
        if(mysqli_num_rows($data) > 0){
            $no = 1;
            while($row = mysqli_fetch_array($data)){
                if($row['stat'] == 1){
                    $statusLaporan =  '<span class="label label-danger">Belum Diverifikasi</span>';
                }
                if($row['stat'] == 2){
                    $statusLaporan =  '<span class="label label-primary">Sudah Diverifikasi</span>';
                }
    ?>
        <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $row['nama_korban'] ?></td>
            <td><?php echo $statusLaporan  ?></td>
        </tr>
    <?php 
          }
        }
    ?>
    </tbody>
</table>
<?php
$data = ob_get_clean();
?>