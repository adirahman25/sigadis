<?php 
session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include './lib/db.php'; 

//Load Composer's autoloader
require 'vendor/autoload.php';
require './lib/config-mail.php';

if(isset($_POST['register'])){

    $mail = new PHPMailer(true); 

    $nama_depan = mysqli_real_escape_string($link,$_POST['nama_depan']);
    $nama_belakang = mysqli_real_escape_string($link,$_POST['nama_belakang']);
    $ktp = mysqli_real_escape_string($link,$_POST['ktp']);
    $email = mysqli_real_escape_string($link,$_POST['email']);
    $username = mysqli_real_escape_string($link,$_POST['username']);
    $password = password_hash($_POST['password'],PASSWORD_DEFAULT) ;
    $user_key = uniqid();

    $queryCheck = "SELECT * FROM users_login WHERE username = '$username' OR email = '$email'";
	$checkResult = mysqli_query($link,$queryCheck);
	$row = mysqli_fetch_array($checkResult);
	if ($row) {
		$result = "Username / Email telah terpakai!";
	}

    if(empty($result)){
        $queryRegister = mysqli_query($link,"INSERT INTO users_login (nama_depan,nama_belakang,ktp,email,username,password,user_key) VALUES ('$nama_depan','$nama_belakang','$ktp','$email','$username','$password','$user_key')");
        
        $emailTo = $email;

	    $id = mysqli_insert_id($link);
	    $subject = '[SIGADIS] Activation Account For: '.$username;
	    $deskripsi = 'Link Activation: http://localhost/webmaterial/verify.php?id='.$id.'&user_key='.$user_key;
    
        //Server settings
	    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = SMTPUSER;                 // SMTP username
	    $mail->Password = SMTPPASS;                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    //Recipients
	    $mail->setFrom('no-reply@accounts.com', 'Activation Account');
	    $mail->addAddress($emailTo);     // Add a recipient
	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = $subject;
	    $mail->Body    = $deskripsi;

	    $mail->send();

		if ($queryRegister) {
			$result= "Berhasil, Silahkan check email anda!";
		}else{
			$result = "Gagal!";
		}
    
    }


   
}

if(isset($_POST['login'])){

    $username = mysqli_real_escape_string($link,$_POST['username']);
    $password = mysqli_real_escape_string($link,$_POST['password']);
    $queryLogin = mysqli_query($link,"SELECT * FROM users_login WHERE username = '$username' AND status = 1 ");
    $row = mysqli_fetch_assoc($queryLogin);

    if($queryLogin){
        $pass = $row['password'];
        if(password_verify($password,$pass)){
            
            $_SESSION['id_user'] = $row['id_login'];
            $_SESSION['namaDepan'] = $row['nama_depan'];
            $_SESSION['namaBelakang'] = $row['nama_belakang'];
            $_SESSION['ktp'] = $row['ktp'];
            $_SESSION['email'] = $row['email'];
            $_SESSION['username'] = $username;
			$_SESSION['password'] = $password;
            header('location:page.php');
            // echo '<script>alert("sukses")</script>';
        }
    }
    else {
        echo '<script>alert("gagal")</script>';
    }
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>Material Style</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/favicon.png?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/preload.min.css" />
    <link rel="stylesheet" href="assets/css/plugins.min.css" />
    <link rel="stylesheet" href="assets/css/style.light-blue-500.min.css" />
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="ms-preload" class="ms-preload">
      <div id="status">
        <div class="spinner">
          <div class="dot1"></div>
          <div class="dot2"></div>
        </div>
      </div>
    </div>
    <div class="bg-full-page ms-hero-bg-dark ms-hero-img-airplane back-fixed">
      <div class="mw-500 absolute-center">
        <div class="card color-dark shadow-6dp animated fadeIn animation-delay-7">
          <div class="ms-hero-bg-primary ms-hero-img-mountain">
            <h2 class="text-center no-m pt-4 color-white index-1">Selamat Datang</h2>
            <h3 class="text-center no-m pt-2 pb-4 color-white index-1">Kabupaten Bogor</h3>
            <!-- <img src="assets/img/logo.png" /> -->
          </div>
          <ul class="nav nav-tabs nav-tabs-full nav-tabs-2 nav-tabs-transparent indicator-primary" role="tablist">
            <li role="presentation" class="active">
              <a href="#ms-login-tab" aria-controls="ms-login-tab" role="tab" data-toggle="tab" class="withoutripple">
                <i class="zmdi zmdi-account"></i> Login</a>
            </li>
            <li role="presentation">
              <a href="#ms-register-tab" aria-controls="ms-register-tab" role="tab" data-toggle="tab" class="withoutripple">
                <i class="zmdi zmdi-account-add"></i> Register</a>
            </li>
            
          </ul>
          <div class="card-block">
            <div class="tab-content">
            <!--Login-->
              <div role="tabpanel" class="tab-pane fade active in" id="ms-login-tab">
                <form method="post">
                  <fieldset>
                    <div class="form-group label-floating">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-account"></i>
                        </span>
                        <label class="control-label" for="ms-form-user">Username</label>
                        <input type="text" id="ms-form-user" class="form-control" name="username"> 
                    </div>
                    </div>
                    
                    <div class="form-group label-floating">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-lock"></i>
                        </span>
                        <label class="control-label" for="ms-form-pass">Password</label>
                        <input type="password" id="ms-form-pass" class="form-control" name="password"> 
                    </div>
                    </div>
                    
                    <div class="row ">
                      <div class="col-xs-12">
                        <button class="btn btn-raised btn-primary btn-block" type="submit" name="login">Login</button>
                      </div>
                    </div>
                  </fieldset>
                </form>
                
              </div>

              <!--Register-->
              <div role="tabpanel" class="tab-pane fade" id="ms-register-tab">
                <form method="post">
                  <fieldset>

                    <div class="form-group label-floating">
                        <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-account"></i>
                        </span>
                        <label class="control-label" for="ms-form-user">Nama Depan</label>
                        <input name="nama_depan" type="text" class="form-control"  required="required">
                        </div>
                    </div>

                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-account"></i>
                        </span>
                        <label class="control-label" for="ms-form-email">Nama Belakang</label>
                        <input name="nama_belakang" type="text" class="form-control"  required="required">
                      </div>
                    </div>

                    <div class="form-group label-floating">
                    <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-account-box-mail"></i>
                        </span>
                        <label class="control-label" for="ms-form-pass">KTP</label>
                        <input name="ktp" type="text" class="form-control"   maxlength="16" required="required">
                    </div>
                    </div>

                    <div class="form-group label-floating">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="zmdi zmdi-email"></i>
                        </span>
                        <label class="control-label" for="ms-form-pass">Email</label>
                        <input name="email" type="email" class="form-control" placeholder="Email" required="required">
                        </div>
                    </div>

                    <div class="form-group label-floating">
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-account"></i>
                        </span>
                        <label class="control-label" for="ms-form-user">Username</label>
                        <input name="username" type="text" class="form-control"  required="required">
                        </div>
                    </div>

                    <div class="form-group label-floating">
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                        </span>
                        <label class="control-label" for="ms-form-email">Password</label>
                        <input name="password" type="password" class="form-control"  required="required">
                        </div>
                    </div>
                    <button class="btn btn-raised btn-block btn-primary" type="submit" name="register">Register Now</button>
                  </fieldset>
                </form>
              </div>

              
            </div>
          </div>
        </div>
        <div class="text-center animated fadeInUp animation-delay-7">
          <a href="forgot.php" class="btn btn-white">
            <i class="zmdi zmdi-key"></i> Lupa Password</a>
        </div>
      </div>
    </div>
    <script src="assets/js/plugins.min.js"></script>
    <script src="assets/js/app.min.js"></script>
  </body>
</html>