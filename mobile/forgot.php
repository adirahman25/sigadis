<?php  
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include './lib/db.php'; 

//Load Composer's autoloader
require 'vendor/autoload.php';
require './lib/config-mail.php';

$userID = isset($_GET['id']) ? $_GET['id'] : null;
$userKey = isset($_GET['user_key']) ? $_GET['user_key'] : null;

$sql = "SELECT * FROM users_login WHERE id_login = '$userID' AND user_key='$userKey'";
$query = mysqli_query($link, $sql);
$row = mysqli_fetch_assoc($query);
if ($userID && $userKey) {
	if ($row) {
		$status = $row['status'];
		$changePassword = true;
	}else{
		echo '<script>alert("gagal kode verifikasi salah")</script>';		
	}
}
if (isset($_POST['forgot'])) {
    $mail = new PHPMailer(true); // Passing `true` enables exceptions

	//CREATE
	$email = $_POST["email"];
	$user_key = uniqid();
	$sqlCheck = "SELECT * FROM users_login WHERE  email = '$email'";
	$queryCheck = mysqli_query($link,$sqlCheck);
	$row = mysqli_fetch_assoc($queryCheck);
	if ($row) {
		$user_keyNow = $row['user_key'];
		$status = $row['status'];
		$emailTo = $row['email'];
		if ($status == 0) {
		    echo '<script>alert("gagal akun anda belum di aktivasi")</script>';	
		}else if (!empty($user_keyNow)) {
		    echo '<script>alert("silahkan cek email anda link sudah kami kirimkan")</script>';		
		}

		
				$id = $row['id_login'];
				$sql = "UPDATE users_login SET user_key = '$user_key' WHERE id_login = '$id'";
				$query = mysqli_query($link,$sql);
				if ($query) {
				    $subject = '[SIGADIS] Forgot Passowrd Account For: '.$email;
				    $deskripsi = 'Link Forgot Passowrd: http://localhost/webmaterial/forgot.php?id='.$id.'&user_key='.$user_key;
				    //Server settings
				    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
				    $mail->isSMTP();                                      // Set mailer to use SMTP
				    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
				    $mail->SMTPAuth = true;                               // Enable SMTP authentication
				    $mail->Username = SMTPUSER;                 // SMTP username
				    $mail->Password = SMTPPASS;                           // SMTP password
				    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
				    $mail->Port = 587;                                    // TCP port to linkect to

				    //Recipients
				    $mail->setFrom('no-reply@accounts.com', 'Forgot Passowrd Account');
				    $mail->addAddress($emailTo);     // Add a recipient
				    //Content
				    $mail->isHTML(true);                                  // Set email format to HTML
				    $mail->Subject = $subject;
				    $mail->Body    = $deskripsi;

				    $mail->send();			
                    echo '<script>alert("berhasil silahkan cek email anda")</script>';	    
				}		
				
		
	}else{
	    echo '<script>alert("email tidak di temukan")</script>';	
	}
}

if (isset($_POST['changepassword'])) {
	$password = $_POST["password"];
	$re_password = $_POST['re-password'];	
	if ($password != $re_password) {
        echo '<script>alert("konfirmasi password tidak sama")</script>'; 
	}
	$User_key = $row['user_key'];
	if (empty($User_key)) {
        echo '<script>alert("kode verifikasi salah")</script>';
	}

	else {
		$id = $row['id_login'];
		$password = password_hash($password, PASSWORD_DEFAULT);
		$sql = "UPDATE users_login SET password = '$password' WHERE id_login = '$id'";
		$query = mysqli_query($link,$sql);
		if ($query) {
		    echo '<script>alert("ganti password berhasil silahkan login")</script>';
		}else{
		    echo '<script>alert("ganti password gagal")</script>';		
		}		
	}
}
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <div class="container">
    	<div class="row justify-content-center align-items-center">
    		<div class="col-6">
    			<h1 class="display-3 text-center">Lupa Password</h1>		
    			
				<form method="post">
                    <?php if (isset($changePassword)): ?>
					<div class="form-group">
						<label>password</label>
						<input type="password" name="password" placeholder="Masukan password...." class="form-control">
					</div>
					<div class="form-group">
						<label>re-password</label>
						<input type="password" name="re-password" placeholder="Masukan re-password...." class="form-control">
					</div>	
					<button type="submit" name="changepassword" class="btn btn-danger btn-block">Change Password</button>
                    <?php else: ?>
					<div class="form-group">
						<label>Masukan Email Anda : </label>
						<input type="text" name="email" placeholder="Masukan email" class="form-control">
					</div>
					<a href="index.php" class="btn btn-primary">Home</a>
					<button type="submit" name="forgot" class="btn btn-danger">Lupa Password</button>						
                    <?php endif ?>
				</form> 
    		</div>
		</div>    	
    </div>
