<?php 
error_reporting("E_ALL ^ E_NOTICE");
include 'lib/db.php';
include 'nav.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    
    <!-- Global Css -->
   
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab|Source+Sans+Pro:400,900" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/hamburgers.min.css">
    <link rel="stylesheet" href="assets/css/offcanvas.css">
    <link rel="stylesheet" href="assets/css/dzsparallaxer.css">
    <link rel="stylesheet" href="assets/css/scroller.css">
    <link rel="stylesheet" href="assets/css/plugin.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link  rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href=" assets/css/typed.css">
    <link rel="stylesheet" href=" assets/css/dataTable.css">
    <!-- <link  rel="stylesheet" href="assets/jquery.fancybox.min.css"> -->
    


    <!-- CSS Unify -->
    <link rel="stylesheet" href="assets/css/unify-core.css">
    <link rel="stylesheet" href="assets/css/unify-components.css">
    <link rel="stylesheet" href="assets/css/unify-globals.css">
    <link rel="stylesheet" href="assets/css/unify.css">

    

</head>
<body style="font-family: 'Josefin Slab', serif;">
<main>
    <!-- Header -->
    <header id="js-header" class="u-header u-header--sticky-top u-header--toggle-section u-header--change-appearance" data-header-fix-moment="300">
      <div class="u-header__section u-header__section--dark  g-transition-0_3 g-py-10" data-header-fix-moment-exclude="g-py-10" data-header-fix-moment-classes="g-py-0" style="background-color:rgb(25, 230, 25,0.8)">
        <nav class="navbar navbar-expand-lg">
          <div class="container">
            <!-- Responsive Toggle Button -->
            <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
              <span class="hamburger hamburger--slider  g-bg-black">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
              </span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Logo -->
            <a href="http://localhost/web" class="navbar-brand">
              <!-- <img src="img/logo.png" alt="Image Description"> -->
            </a>
            <!-- End Logo -->

            <!-- Navigation -->
            <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg" id="navBar">
              <ul class="navbar-nav text-uppercase g-font-weight-600 ml-auto">
                <li class="nav-item g-mx-20--lg">
                    <a href="?page=home" class="nav-link px-0 g-color-white">Home</a>
                </li>
                <li class="nav-item g-mx-20--lg">
                    <a href="?page=profile" class="nav-link px-0 g-color-white">Profile</a>
                </li>
                <!-- <li class="nav-item g-mx-20--lg active g-color-black">
                    <a href="?page=kie" class="nav-link px-0 g-color-black">Komunikasi Informasi dan Edukasi</a>
                </li>
                <li class="nav-item g-mx-20--lg">
                    <a href="#" class="nav-link px-0 g-color-black">Program</a>
                </li>
                <li class="nav-item g-mx-20--lg">
                    <a href="#" class="nav-link px-0 g-color-black">Informasi</a>
                </li> -->
              </ul>
            </div>
            <!-- End Navigation -->
          </div>
        </nav>
      </div>
    </header>
    <!-- End Header -->

    <!-- Main Body -->
        <?php echo $content; ?>
    <!--------------->


  <!-- Footer -->
 <div id="contacts-section" class="g-bg-black-opacity-0_9 g-color-white-opacity-0_8 g-py-60">
      <div class="container">
        <div class="row">
          <!-- Footer Content -->
          <div class="col-lg-4 col-md-6 g-mb-40 g-mb-0--lg">
            <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
              <h2 class="u-heading-v2__title h6 text-uppercase mb-0">DP3AP2KB</h2>
            </div>

            <p>
            Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana.
            </p>
          </div>
          <!-- End Footer Content -->

          <!-- Footer Content -->
          <div class="col-lg-4 col-md-6 g-mb-40 g-mb-0--lg">
            <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
              <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Artikel</h2>
            </div>

            <article>
              <h3 class="h6 g-mb-2">
            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">Incredible template</a>
          </h3>
              <div class="small g-color-white-opacity-0_6">May 8, 2017</div>
            </article>

            <hr class="g-brd-white-opacity-0_1 g-my-10">

            <article>
              <h3 class="h6 g-mb-2">
            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">New features</a>
          </h3>
              <div class="small g-color-white-opacity-0_6">June 23, 2017</div>
            </article>

            <hr class="g-brd-white-opacity-0_1 g-my-10">

            <article>
              <h3 class="h6 g-mb-2">
            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#">New terms and conditions</a>
          </h3>
              <div class="small g-color-white-opacity-0_6">September 15, 2017</div>
            </article>
          </div>
          <!-- End Footer Content -->

          

          <!-- Footer Content -->
          <div class="col-lg-4 col-md-6">
            <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
              <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Kontak Kami</h2>
            </div>

            <address class="g-bg-no-repeat g-font-size-12 mb-0" style="background-image: url(../../assets/img/maps/map2.png);">
          <!-- Location -->
          <div class="d-flex g-mb-20">
            <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-map-marker"></i>
              </span>
            </div>
            <p class="mb-0">Jalan Tegar Beriman No.1,
            Pakansari, Cibinong,
            <br> Bogor, Jawa Barat 16914
            </p>
          </div>
          <!-- End Location -->

          <!-- Phone -->
          <div class="d-flex g-mb-20">
            <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-phone"></i>
              </span>
            </div>
            <p class="mb-0">(+62)21 8758605</p>
          </div>
          <!-- End Phone -->

          <!-- Email and Website -->
          <div class="d-flex g-mb-20">
            <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-globe"></i>
              </span>
            </div>
            <p class="mb-0">
              <a class="g-color-white-opacity-0_8 g-color-white--hover" href="http://dp3ap2kb.bogorkab.go.id/">dp3ap2kb.bogorkab.go.id</a>
              <br>
              <a class="g-color-white-opacity-0_8 g-color-white--hover" href="http://sigadis.bogorkab.go.id/">sigadis.bogorkab.go.id</a>
            </p>
          </div>
          <!-- End Email and Website -->
        </address>
          </div>
          <!-- End Footer Content -->
        </div>
      </div>
    </div>
    <!-- End Footer -->

    <!-- Copyright Footer -->
    <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
            <div class="d-lg-flex">
              <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md"><?php echo date('Y') ?> © Sigadis Kabupaten Bogor</small>
             
            </div>
          </div>
          

          <!-- Media Sosial -->
          <!-- <div class="col-md-4 align-self-center">
            <ul class="list-inline text-center text-md-right mb-0">
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
                <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Skype">
                <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-skype"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Linkedin">
                <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Pinterest">
                <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-pinterest"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
                <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Dribbble">
                <a href="#" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-dribbble"></i>
                </a>
              </li>
            </ul>
          </div> -->

        </div>
      </div>
    </footer>




    </main>
    <!-- JS Global Compulsory -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery-migrate.min.js"></script>
    <script src="assets/js/jquery.easing.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/offcanvas.js"></script>


    <!--Implementing Plugin-->
    <script src="assets/js/slick.js"></script>
    <script src="assets/js/dzsparallaxer.js"></script>
    <script src="assets/js/scroller.js"></script>
    <script src="assets/js/plugin.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
    <script src="assets/js/typed.min.js"></script>
    <!-- <script  src="assets/js/jquery.fancybox.min.js"></script> -->

    <!-- JS Unify -->
    <script src="assets/js/hs.core.js"></script>
    <script src="assets/js/hs.carousel.js"></script>
    <script src="assets/js/hs.header.js"></script>
    <script src="assets/js/hs.hamburgers.js"></script>
    <script  src="assets/js/hs.popup.js"></script>
    <script src="assets/js/hs.text-slideshow.js"></script>
    <!-- Plugn -->
    <script src="assets/js/dataTable.js"></script>
    <script>
        $(document).ready(function(){

            //initialize carousel news
            $.HSCore.components.HSCarousel.init('.js-carousel');


            //  initialization of popups
            // $.HSCore.components.HSPopup.init('.js-fancybox');


            $('#promo-carousel').slick('setOption', 'responsive', [{
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
                centerPadding: '30px'
              }
            }, {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                centerPadding: '60px'
              }
            }, {
              breakpoint: 650,
              settings: {
                slidesToShow: 1,
                centerPadding: '40px'
              }
            }], true);
            
            
            
            // initialization of text animation (typing)
            $(".u-text-animation.u-text-animation--typing").typed({
              strings: [
                "Pengaduan",
                "Dan Data Informasi Genders"
              ],
              typeSpeed: 60,
              loop: true,
              backDelay: 1500
            });
            $(document).ready( function () {
                $('#kieTable').DataTable();
            });
            
        });
        $(window).on('load', function () {
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
        });
    </script>

    
</body>
</html>