<?php
session_start();
if (empty($_SESSION)){
	header('location:index.php');
}
elseif (isset($_POST['logout']))
{
	session_destroy();

	// kembali ke halaman form login
	header("location:index.php");
}
else {


error_reporting("E_ALL ^ E_NOTICE");
include 'lib/db.php';
include './navigasi.php';

?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Dashboard</title>
     <link rel="shortcut icon" href="img/icon.png">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Toastr style -->
    <link href="assets/css/toastr.min.css" rel="stylesheet">
	<link href="assets/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="assets/css/sweetalert.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <!-- Gritter -->
    <link href="assets/css/bootstrap-chosen.css" rel="stylesheet">
    <link href="assets/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/jquery.gritter.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>

<body>
<div id="wrapper" style="background-color:#ff33cc;">
        <nav class="navbar-default navbar-static-side" role="navigation" >
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu" >
                    <li class="nav-header" style="background-color:#ff33cc;">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-fluid" src="img/logo.png" />
                             </span>
                            
                        </div>
                      
                    </li>
                    <hr>
                    <li>
                        <a style="color:#fff;" href="?page=home"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li>
                        <a style="color:#fff;" href="#"><i class="fa fa-file"></i> <span class="nav-label">Data</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="?page=kasus">Data Kasus</a></li>
                            <li><a href="?page=laporan">Laporan Kasus</a></li>
                        </ul>
                    </li>
                    <li>
                        <a style="color:#fff;" href="#"><i class="fa fa-file"></i> <span class="nav-label">Publikasi</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="?page=photo">Galeri Foto</a></li>
                            <li><a href="?page=artikel">Artikel</a></li>
                            <li><a href="?page=video">Galeri Video</a></li>
                        </ul>
                    </li>
                    <li>
                        <a style="color:#fff;" href="#"><i class="fa fa-file"></i> <span class="nav-label">Informasi</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="?page=unitKerja">Unit Kerja</a></li>
                            <li><a href="?page=gender">Gender</a></li>
                            <li><a href="?page=artikel">Organisasi Perempuan</a></li>
                            <li><a href="?page=video">Peraturan </a></li>
                        </ul>
                    </li>
                    <li>
                        <a style="color:#fff;" href="#"><i class="fa fa-file"></i> <span class="nav-label">Pengaduan</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="?page=pengaduan">Data Pengaduan</a></li>
                            
                        </ul>
                    </li>
                    <!-- <li>
                        <a style="color:#fff;" href="#"><i class="fa fa-file"></i> <span class="nav-label">Kontent</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="graph_flot.html">Kekerasan Terhadap Anak</a></li>
                            <li><a href="graph_morris.html">Kekerasan Terhadap Perempuan</a></li>
                            <li><a href="graph_flot.html">KDRT</a></li>
                            <li><a href="graph_flot.html">Perdagangan Orang</a></li>
                            <li><a href="graph_flot.html">Psikologi</a></li>
                        </ul>
                    </li> -->
                    <!-- <li>
                        <a style="color:#fff;" href="#"><i class="fa fa-file"></i> <span class="nav-label">Anak Hilang</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="graph_flot.html">Daftar Anak Hilang</a></li>
                            <li><a href="graph_morris.html">Daftar Anak Ditemukan</a></li>
                        </ul>
                    </li> -->
                    <hr>
                    <li>
                        <a style="color:#fff;" href="layouts.html"><i class="fas fa-cogs"></i> <span class="nav-label">Setting</span></a>
                    </li>
                  
                </ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-danger " href="#"><i class="fa fa-bars"></i> </a>                        
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        


                        <li>
                            <form method="post" style="padding-top:10px;">
                                <button class="btn  btn-block" style="background-color:#444;color:#fff;" type="submit" name="logout"><i class="fas fa-sign-out-alt"></i> Logout</button>
                            </form>
                        </li>
                        
                    </ul>
                </nav>
            </div>
            <?php echo $content; ?>
        </div>
    
</div>

    <!-- Mainly scripts -->
   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.metisMenu.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="assets/js/inspinia.js"></script>
    <script src="assets/js/pace.min.js"></script>
    <script src="assets/js/chosen.jquery.js"></script>
     <!-- jQuery UI -->
    <script src="assets/js/jquery-ui.min.js"></script>

    <!--Custom Plugin-->
    <script src="assets/js/sweetalert.min.js"></script>
    <script src="assets/js/jasny-bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=3rnrnjlv514xwwee3bl8gux9dru4ptxkevjwcfy9jdgbdxz1"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
    <script>
      Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Grafik Data Kasus Kekerasan'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Total',
                colorByPoint: true,
                 data: [<?= join($arrayPie,",") ?>],
                        
            }]
        });
    </script>
    <script>
        
        $(document).ready( function () {
            $('#example').DataTable();
            $('#gender').DataTable();     
            $('#unitkerja').DataTable();    
            $('.chosen-select').chosen({width: "100%"});
        });
    </script>

	
		
</body>
</html>
<?php 
}
?>
