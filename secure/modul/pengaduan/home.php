<?php
ob_start();
?>
<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Data Laporan Masyarakat</h1>
        <h1 class="text-center">Kabupaten Bogor</h1>
        <table id="gender" class="display" style="width:100%">
            <thead>
                    <tr>
                        <th>Nama Lengkap</th>
                        <th>No. KTP</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    
                </thead>
                <tbody>
                <?php 
                    $data = mysqli_query($link,"SELECT * FROM tm_pengaduan INNER JOIN users_login ON users_login.id_login = tm_pengaduan.id_login");
                    while($row = mysqli_fetch_array($data)) {
                        
                        if($row['stat'] == "1"){
                            $status = "<span class=\"label label-danger\">Belum Diverifikasi</span>";
                        }
                        else if($row['stat'] == "2") {
                            $status = "<span class=\"label label-primary\">Sudah Diverifikasi</span>";
                        }

                ?>
                    <tr>
                        
                        <td><?php echo strtoupper($row['nama_depan'] . ' ' . $row['nama_belakang'])  ?></td>
                        <td><?php echo $row['ktp'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td><?php echo $status ?></td>
                        <td>
                            <a class="btn btn-info btn-outline" href="?page=editStatus&id=<?php echo $row['id_pengaduan'] ?>"">Detail</a>
                        </td>
                        
                    </tr>
                    <?php 
                    }
                    ?>
                </tbody>
        </table>
    </div>
    
    
</div>  
<?php 
$homePengaduan = ob_get_clean();
?>