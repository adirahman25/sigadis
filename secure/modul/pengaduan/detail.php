<?php 
ob_start();
$id = $_GET['id'];


if(isset($_POST['update'])){
    $stat = mysqli_real_escape_string($link,$_POST['stat']);

    $queryUpdateText = mysqli_query($link,"UPDATE tm_pengaduan SET stat = '$stat' WHERE tm_pengaduan.id_pengaduan ='$id'");
    
    if($queryUpdateText){
        echo '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Sukses!</strong> Update Status Pelaporan.
            </div>';
    }
    else {
        echo '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Gagal!</strong> Update Status Pelaporan.
            </div>';
    }
}


$data = mysqli_query($link,"SELECT * FROM tm_pengaduan  INNER JOIN users_login ON users_login.id_login = tm_pengaduan.id_login where id_pengaduan = '$id'");
$row = mysqli_fetch_array($data);
    if($row['stat'] == "1"){
        $status = "<span class=\"label label-danger\">Belum Diverifikasi</span>";
    }
    else if($row['stat'] == "2") {
        $status = "<span class=\"label label-primary\">Sudah Diverifikasi</span>";
    }
    if($row['jenis_kelamin'] == "1"){
        $jk = "<span class=\"label label-info\">Laki - Laki</span>";
    }
    else if($row['jenis_kelamin'] == "2") {
        $jk = "<span class=\"label label-success\">Perempuan</span>";
    }
?>
<h2 class="text-center">Detail Laporan Masyarakat</h2>
<div class="ibox-content">
    <div class="row">
        <div class="col-md-4">
        <h3>Biodata</h3>
            <table class="table">
                <tr>
                    <td>Nama Lengkap</td>
                    <td><?php echo strtoupper($row['nama_depan'] . ' ' . $row['nama_belakang'])  ?></td>
                </tr>
                <tr>
                    <td>Nomor KTP</td>
                    <td><?php echo $row['ktp']  ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?php echo $row['email']  ?></td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td><?php echo strtoupper($row['username']) ?></td>
                </tr>
            </table>
        </div>
        <div class="col-md-8">
        <h3>Laporan</h3>
            <table class="table">
                <tr>
                    <td>Alamat</td>
                    <td><?php echo $row['alamat'] ?></td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td><?php echo $row['telp']  ?></td>
                </tr>
               
                <tr>
                    <td>Nama Korban</td>
                    <td><?php echo strtoupper($row['nama_korban']) ?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td><?php echo $jk ?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td><?php echo $row['usia_korban'] ?></td>
                </tr>
                <tr>
                    <td>Photo Laporan</td>
                    <td><img src="fileSecret/<?php echo $row['files'] ?>"></td>
                </tr>
                <tr>
                    <td>Deskripsi</td>
                    <td><?php echo $row['deskripsi'] ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?php echo $status ?></td>
                </tr>
                <tr>
                    <td>Ubah Status</td>
                    <td>
                        <form method="post">
                            <div class="form-group">
                                <select name="stat" class="form-control">
                                    <option value="1" <?php if($row['stat'] == "1"){ echo "selected";} ?>>Belum Di Verifikasi</option>
                                    <option value="2" <?php if($row['stat'] == "2"){ echo "selected";} ?>>Sudah Verifikasi</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="update" class="btn btn-danger" value="Update">
                            </div>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php 
$detailPengaduan = ob_get_clean();
?>