<?php 
ob_start();
$id = $_GET['id'];
if(isset($_POST['update'])){

    $tanggal = mysqli_real_escape_string($link,$_POST['tgl_upload']);
    $judul= mysqli_real_escape_string($link,$_POST['judul']);

    $targetDir = "fileSecret/";
    $filename = uniqid() . rand(1,99999) . $_FILES["files"]["name"];
    // list($width, $height, $type, $attr) = getimagesize($filename);
    $targetFilePath = $targetDir .  $filename  ;
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
    // $source = $_FILES["file"]["tmp_name"]  ;
    
    if(!empty($_FILES["files"]["name"])){
        //izinkan file yang diupload

        $allowType = array("jpg","png","jpeg","JPG");
        if(in_array($fileType,$allowType)){
            //upload to server
            // if($_FILES['fl_photo']['size'] < 10000000){
                    
                    if(move_uploaded_file($_FILES["files"]["tmp_name"], $targetFilePath)){
                        $data = mysqli_query($link,"select * from tm_foto where id_foto = '$id'");
                        $row = mysqli_fetch_array($data);
                        $delfoto = unlink("fileSecret/" . $row['files']);
                        
                        $query = mysqli_query($link,"UPDATE tm_foto SET tgl_upload='$tanggal',judul='$judul',files='$filename' WHERE tm_foto.id_foto='$id'");
                        // print_r($query);
                        if($query){
                            echo "sukses";
                        }
                        else {
                            echo "gagal";
                        }
                    }
                    else {
                        echo "gagal upload ke folder";
                        
                    }
            // }
            // else {
            //     echo "<script>alert('ukuran harus dibawah banget')</script>";
            // }   
        }
        else {
            echo "pastikan format anda benar";
        }
    }
    else {
        $queryUpdateText = mysqli_query($link,"UPDATE tm_foto SET tgl_upload='$tanggal',judul='$judul' WHERE tm_foto.id_foto='$id'");
        if($queryUpdateText){
            echo 'sukses';
        }
        else {
            echo 'gagal';
        }
    }

}
else {
    $data = mysqli_query($link,"SELECT * FROM tm_foto WHERE tm_foto.id_foto='$id'");
    $row = mysqli_fetch_array($data);
?>

<div class="row" style="margin-top:20px;">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Form Update Foto</h5>
            </div>
            <div class="ibox-content">
                <form method="post" enctype="multipart/form-data" class="form-horizontal">
                        <div class="form-group">
                            
                            <label class="col-sm-2 control-label">Tanggal Upload</label>
                            <div class="col-sm-4">
                                <input type="date" class="form-control" name="tgl_upload" value="<?php echo $row['tgl_upload'] ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Judul Foto</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="judul" value="<?php echo $row['judul'] ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Upload Image</label>
                            <div class="col-sm-10">
                                <input type="file" name="files"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Preview Image</label>
                            <div class="col-sm-10">
                                <img src="fileSecret/<?php echo $row['files'] ?>" class="img-fluid" style="width:300px;height:200px;"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-md-offset-2">
                                <input type="submit" class="btn btn-danger"  name="update" value="Update">
                            </div>
                        </div> 
                </form>
            </div>

        </div>
    </div>
</div>

<?php 
}
$editPhoto = ob_get_clean();
?>