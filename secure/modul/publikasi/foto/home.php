<?php
ob_start();

/**
 * Query Upload Foto 
 */
if(isset($_POST['kirim'])){
   
    
    $tanggal = mysqli_real_escape_string($link,$_POST['tgl_upload']);
    $judul= mysqli_real_escape_string($link,$_POST['judul']);

    $targetDir = "fileSecret/";
    $filename = uniqid() . rand(1,99999) . $_FILES["files"]["name"];
    // list($width, $height, $type, $attr) = getimagesize($filename);
    $targetFilePath = $targetDir .  $filename  ;
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
    // $source = $_FILES["file"]["tmp_name"]  ;
    
    if(isset($_POST['kirim']) && !empty($_FILES["files"]["name"])){
        //izinkan file yang diupload

        $allowType = array("jpg","png","jpeg","JPG");
        if(in_array($fileType,$allowType)){
            //upload to server
            // if($_FILES['fl_photo']['size'] < 10000000){
                    
                    if(move_uploaded_file($_FILES["files"]["tmp_name"], $targetFilePath)){
                        $query = mysqli_query($link,"INSERT INTO tm_foto (tgl_upload,judul,files) VALUES ('$tanggal','$judul','$filename')");
                        // print_r($query);
                        if($query){
                            echo "<script>swal('gagal ke db')</script>";
                        }
                        else {
                            echo "<script>swal('gagal ke db')</script>";
                        }
                    }
                    else {
                        echo "<script>swal('gagal upload ke folder')</script>";
                        
                    }
            // }
            // else {
            //     echo "<script>alert('ukuran harus dibawah banget')</script>";
            // }   
        }
        else {
            echo "<script>swal('psatikan format jpg,png,jpeg,JPG')</script>";
        }
    }
    else {
        echo "<script>swal('silahkan pilih file')</script>";
    }

    // $query = mysqli_query($link,"INSERT INTO tm_foto (tgl_upload,judul) VALUES ('$tanggal','$judul')");
    //                     // print_r($query);
    //                     if($query){
    //                         echo "<script>alert('sukses')</script>";
    //                     }
    //                     else {
    //                         echo "<script>alert('gagal ke db')</script>";
    //                     }

}


?>
<button id="addPhoto" class="btn btn-primary dim" type="button" data-toggle="modal" data-target="#myModal">
    <i class="fa fa-plus"></i> Data Foto
</button>   
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!-- <i class="fa fa-pencil modal-icon"></i> -->
                <h4 class="modal-title"></h4>
                <h3 class="font-bold">
                    Form Photo
                </h3>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label">Tanggal Upload</label>
                            <input type="date" class="form-control" name="tgl_upload"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Judul Foto</label>
                            <input type="text" class="form-control" name="judul"/>
                        </div>
                        <div class="form-group">
                            <!-- <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
                                <input type="file" name="fl_photo" accept="image/*"></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                            </div> -->
                            <input type="file" name="files"/>
                        </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" id="kirim" name="kirim" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <table id="example" class="display" style="width:100%">
        <thead>
                <tr>
                    <th>Tanggal Upload</th>
                    <th>Judul</th>
                    <!-- <th>Image</th> -->
                    <th>Action</th>
                 
                </tr>
            </thead>
            <tbody>
            <?php 
                $data = mysqli_query($link,"SELECT tm_foto.id_foto, tm_foto.tgl_upload,tm_foto.judul,tm_foto.files FROM tm_foto");
                while($row = mysqli_fetch_array($data)) {
                    

            ?>
                <tr>
                    <td><?php echo $row['tgl_upload'] ?></td>
                    <td><?php echo $row['judul'] ?></td>
                    <!-- <td><img src="fileSecret/<?php echo $row['files'] ?>"</td> -->
                    <td>
                        <a class="btn btn-primary btn-outline" href="?page=editPhoto&id=<?php echo $row['id_foto'] ?>"">Edit</a>
                        <a class="btn btn-danger btn-outline" href="?page=deletePhoto&id=<?php echo $row['id_foto'] ?>">Delete</a>
                    </td>
                    
                </tr>
                <?php 
                }
                ?>
            </tbody>
    </table>
</div>
<?php 
$photo = ob_get_clean();
?>