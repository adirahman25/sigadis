<?php 
ob_start();
$id = $_GET['id'];
if(isset($_POST['update'])){

    $tanggal = mysqli_real_escape_string($link,$_POST['tgl_upload']);
    $judul= mysqli_real_escape_string($link,$_POST['judul']);
    $deskripsi = $_POST['deskripsi'];

    $targetDir = "fileSecret/";
    $filename = uniqid() . rand(1,99999) . $_FILES["files"]["name"];
    // list($width, $height, $type, $attr) = getimagesize($filename);
    $targetFilePath = $targetDir .  $filename  ;
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
    // $source = $_FILES["file"]["tmp_name"]  ;
    
    if(!empty($_FILES["files"]["name"])){
        //izinkan file yang diupload

        $allowType = array("jpg","png","jpeg","JPG");
        if(in_array($fileType,$allowType)){
            //upload to server
            // if($_FILES['fl_photo']['size'] < 10000000){
                    
                    if(move_uploaded_file($_FILES["files"]["tmp_name"], $targetFilePath)){
                        $data = mysqli_query($link,"select * from tm_artikel where id_artikel = '$id'");
                        $row = mysqli_fetch_array($data);
                        $delfoto = unlink("fileSecret/" . $row['files']);
                        
                        $query = mysqli_query($link,"UPDATE tm_artikel SET tgl_upload='$tanggal',files='$filename',judul='$judul',deskripsi='$deskripsi' WHERE tm_artikel.id_artikel='$id'");
                        // print_r($query);
                        if($query){
                            echo "sukses";
                        }
                        else {
                            echo "gagal";
                        }
                    }
                    else {
                        echo "gagal upload ke folder";
                        
                    }
            // }
            // else {
            //     echo "<script>alert('ukuran harus dibawah banget')</script>";
            // }   
        }
        else {
            echo "pastikan format anda benar";
        }
    }
    else {
        $queryUpdateText = mysqli_query($link,"UPDATE tm_artikel SET tgl_upload='$tanggal',judul='$judul',deskripsi='$deskripsi' WHERE tm_artikel.id_artikel='$id'");
        if($queryUpdateText){
            echo 'sukses';
        }
        else {
            echo 'gagal';
        }
    }

}
else {
    $data = mysqli_query($link,"SELECT * FROM tm_artikel WHERE tm_artikel.id_artikel='$id'");
    $row = mysqli_fetch_array($data);
?>

<form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label class="control-label">Tanggal Upload</label>
            <input type="date" class="form-control" name="tgl_upload" value="<?php echo $row['tgl_upload'] ?>"/>
        </div>
        <div class="form-group">
            <label class="control-label">Cover Image</label>
            <input type="file" name="files"/>
        </div>
        <div class="form-group">
            <label class="control-label">Judul Video</label>
            <input type="text" class="form-control" name="judul" value="<?php echo $row['judul'] ?>"/>
        </div>
        <div class="form-group">
            <label class="control-label">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" cols="15" rows="20"><?php echo $row['deskripsi'] ?></textarea>
        </div>
    
    <div class="modal-footer">
        <input type="submit" class="btn btn-warning" id="kirim" name="update" value="Update">
    </div>
</form>

<?php 
}
$editArtikel = ob_get_clean();
?>