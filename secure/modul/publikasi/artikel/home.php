<?php 
ob_start();

/**
 * Query Upload Artikel
 */

if(isset($_POST['send'])){
   
    
    $tanggal = mysqli_real_escape_string($link,$_POST['tgl_upload']);
    $judul= mysqli_real_escape_string($link,$_POST['judul']);
    $deskripsi = $_POST['deskripsi'];

    $targetDir = "fileSecret/";
    $filename = uniqid() . rand(1,99999) . $_FILES["files"]["name"];
    // list($width, $height, $type, $attr) = getimagesize($filename);
    $targetFilePath = $targetDir .  $filename  ;
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
    // $source = $_FILES["file"]["tmp_name"]  ;
    
    if(!empty($_FILES["files"]["name"])){
        //izinkan file yang diupload

        $allowType = array("jpg","png","jpeg","JPG");
        if(in_array($fileType,$allowType)){
            //upload to server
            // if($_FILES['fl_photo']['size'] < 10000000){
                    
                    if(move_uploaded_file($_FILES["files"]["tmp_name"], $targetFilePath)){
                        $query = mysqli_query($link,"INSERT INTO tm_artikel (tgl_upload,files,judul,deskripsi) VALUES ('$tanggal','$filename','$judul','$deskripsi')");
                        // print_r($query);
                        if($query){
                            echo "sukses";
                        }
                        else {
                            echo "gagal";
                        }
                    }
                    else {
                        echo "gagal upload ke folder";
                        
                    }
            // }
            // else {
            //     echo "<script>alert('ukuran harus dibawah banget')</script>";
            // }   
        }
        else {
            echo "pastikan format jpg,png,jpeg,JPG";
        }
    }
    else {
        echo "silahkan pilih file";
    }
}


?>
<button id="test" class="btn btn-primary dim" type="button" data-toggle="modal" data-target="#addArtikel">
    <i class="fa fa-plus"></i> Data Artikel
</button>   
<div class="modal inmodal" id="addArtikel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!-- <i class="fa fa-pencil modal-icon"></i> -->
                <h4 class="modal-title"></h4>
                <h3 class="font-bold">
                    Form Data Artikel
                </h3>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label">Tanggal Upload</label>
                            <input type="date" class="form-control" name="tgl_upload"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Cover Image</label>
                            <input type="file" name="files"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Judul Video</label>
                            <input type="text" class="form-control" name="judul"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Deskripsi</label>
                            <textarea class="form-control" name="deskripsi" cols="15" rows="15"></textarea>
                        </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" id="kirim" name="send" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <table id="example" class="display" style="width:100%">
        <thead>
                <tr>
                    <th>Tanggal Upload</th>
                    <th>Judul</th>
                    <th>Action</th>
                 
                </tr>
            </thead>
            <tbody>
            <?php 
                $data = mysqli_query($link,"SELECT * FROM tm_artikel");
                while($row = mysqli_fetch_array($data)) {
                    

            ?>
                <tr>
                    <td><?php echo $row['tgl_upload'] ?></td>
                    <td><?php echo $row['judul'] ?></td>
                    <td>
                        <a class="btn btn-primary btn-outline" href="?page=editArtikel&id=<?php echo $row['id_artikel'] ?>"">Edit</a>
                        <a class="btn btn-danger btn-outline" href="?page=deleteArtikel&id=<?php echo $row['id_artikel'] ?>">Delete</a>
                        
                    </td>
                    
                </tr>
                <?php 
                }
                ?>
            </tbody>
    </table>
</div>
<?php 
$artikel = ob_get_clean();
?>