<?php 
ob_start();

/**
 * Query upload video 
 */

 if(isset($_POST['kirim'])){

    $tanggal = mysqli_real_escape_string($link,$_POST['tgl_upload']);
    $judul= mysqli_real_escape_string($link,$_POST['judul']);
    $keyVideo = mysqli_real_escape_string($link,$_POST['key_video']);

    $query = mysqli_query($link,"INSERT INTO tm_video (tgl_upload,judul,key_video) VALUES ('$tanggal','$judul','$keyVideo')");
    if($query){
        echo 'sukses';
    }
    else {
        echo 'gagal';
    }

 }
?>
<button id="addPhoto" class="btn btn-primary dim" type="button" data-toggle="modal" data-target="#myModal">
    <i class="fa fa-plus"></i> Data Video
</button>   
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!-- <i class="fa fa-pencil modal-icon"></i> -->
                <h4 class="modal-title"></h4>
                <h3 class="font-bold">
                    Form Photo
                </h3>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label">Tanggal Upload</label>
                            <input type="date" class="form-control" name="tgl_upload"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Judul Video</label>
                            <input type="text" class="form-control" name="judul"/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Key Video</label>
                            <input type="text" class="form-control" name="key_video"/>
                        </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" id="kirim" name="kirim" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <table id="example" class="display" style="width:100%">
        <thead>
                <tr>
                    <th>Tanggal Upload</th>
                    <th>Judul</th>
                    <th>Key Video</th>
                    <th>Action</th>
                 
                </tr>
            </thead>
            <tbody>
            <?php 
                $data = mysqli_query($link,"SELECT * FROM tm_video");
                while($row = mysqli_fetch_array($data)) {
                    

            ?>
                <tr>
                    <td><?php echo $row['tgl_upload'] ?></td>
                    <td><?php echo $row['judul'] ?></td>
                    <td><?php echo $row['key_video'] ?></td>
                    <td>
                        <a class="btn btn-primary btn-outline" href="?page=editVideo&id=<?php echo $row['id_video'] ?>"">Edit</a>
                        <a class="btn btn-danger btn-outline" href="?page=deleteVideo&id=<?php echo $row['id_video'] ?>">Delete</a>
                        <a class="btn btn-info btn-outline" href="?page=viewVideo&id=<?php echo $row['id_video'] ?>">View</a>
                    </td>
                    
                </tr>
                <?php 
                }
                ?>
            </tbody>
    </table>
</div>
<?php 
$video = ob_get_clean();
?>