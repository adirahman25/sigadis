<?php 
ob_start();

$id = $_GET['id'];
$data = mysqli_query($link,"SELECT * FROM tm_video where id_video='$id'");
$row = mysqli_fetch_array($data);
?>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h1>View Video</h1>
    </div>
    <div class="ibox-content">
        <h2 class="text-center"><?php echo strtoupper($row['judul'] ) ?></h2>
        <figure>
            <iframe width="100%" height="380" src="http://www.youtube.com/embed/<?php echo $row['key_video'] ?>" frameborder="0" allowfullscreen></iframe>
        </figure>
    </div>
</div>
<?php 
$viewVideo = ob_get_clean();
?>