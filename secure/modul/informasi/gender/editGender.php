<?php 
ob_start();
?>
<div class="container">
<div class="row">
    <form method="post" enctype="multipart/form-data">
                    
        <div class="form-group">
           
            <label class="control-label">Unit Kerja</label>
                <select data-placeholder="Choose a Country..." class="chosen-select"  tabindex="2" name="id_unit">
                    <option>-</option>
                    <?php 
                    $data = mysqli_query($link,"SELECT * FROM unit_kerja");
                    while($row = mysqli_fetch_array($data)){    
                    ?>
                    
                    <option value="<?php echo $row['id_unit'] ?>"><?php echo $row['nama_unit'] ?></option>
                    <?php 
                    }
                    ?>
                </select>
            
        </div>
        <div class="form-group">
            <label class="control-label">Jumlah Laki - Laki</label>
            <input type="text" class="form-control" name="male" required/>
        </div>
        <div class="form-group">
            <label class="control-label">Jumlah Perempuan</label>
            <input type="text" class="form-control" name="female" required/>
        </div>
        <div class="form-group">
            <label class="control-label">Tahun Laporan</label>
            <select class="form-control" name="tahun">
                <option value="1">2015</option>
                <option value="2">2016</option>
                <option value="3">2017</option>
                <option value="4">2018</option>
                <option value="5">2019</option>
                <option value="6">2020</option>
                <option value="7">2021</option>
                <option value="8">2022</option>
                <option value="9">2023</option>
            </select>
        </div>

        <div class="form-group">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary" id="kirim" name="kirim" value="Update">
        </div>
    </form>
</div>
</div>
<?php
$editGender = ob_get_clean();
?>