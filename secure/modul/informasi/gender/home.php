<?php
ob_start();
if(isset($_POST['kirim'])){
    $idUnit = mysqli_real_escape_string($link,$_POST['id_unit']);
    $male = mysqli_real_escape_string($link,$_POST['male']);
    $female = mysqli_real_escape_string($link,$_POST['female']);
    $total = $male + $female ;
    $tahun = mysqli_real_escape_string($link,$_POST['tahun']);

    
    $query = mysqli_query($link,"INSERT INTO tm_gender (id_unit,male,female,total,tahun) 
                VALUES ('$idUnit','$male','$female','$total','$tahun')");
    if($query){
        echo 'sukses';
    }
    else {
        echo 'gagal';
    }

}

?>
<button id="addPhoto" class="btn btn-primary dim" type="button" data-toggle="modal" data-target="#myModal">
    <i class="fa fa-plus"></i> Data Informasi Gender
</button>   
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!-- <i class="fa fa-pencil modal-icon"></i> -->
                <h4 class="modal-title"></h4>
                <h3 class="font-bold">
                    Form Informasi Gender
                </h3>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label class="control-label">Unit Kerja</label>
                        <select data-placeholder="Choose a Country..." class="chosen-select"  tabindex="2" name="id_unit">
                            <option>-</option>
                            <?php 
                            $data = mysqli_query($link,"SELECT * FROM unit_kerja");
                            while($row = mysqli_fetch_array($data)){    
                            ?>
                            
                            <option value="<?php echo $row['id_unit'] ?>"><?php echo $row['nama_unit'] ?></option>
                            <?php 
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Jumlah Laki - Laki</label>
                        <input type="text" class="form-control" name="male" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Jumlah Perempuan</label>
                        <input type="text" class="form-control" name="female" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Tahun Laporan</label>
                        <select class="form-control" name="tahun">
                            <option value="1">2015</option>
                            <option value="2">2016</option>
                            <option value="3">2017</option>
                            <option value="4">2018</option>
                            <option value="5">2019</option>
                            <option value="6">2020</option>
                            <option value="7">2021</option>
                            <option value="8">2022</option>
                            <option value="9">2023</option>
                        </select>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" id="kirim" name="kirim" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <!-- <div class="col-md-12">
        <h3>Grafik </h3>
        <div id="container" style="width:100%; height: 400px; margin: 0 auto"></div>
    </div> -->
</div>

<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Jumlah ASN Berdasarkan Jenis Kelamin</h1>
        <h1 class="text-center">Kabupaten Bogor</h1>
        <table id="gender" class="display" style="width:100%">
            <thead>
                    <tr>
                        <th rowspan="2">Tahun Laporan</th>
                        <th rowspan="2">Unit Kerja</th>
                        <!-- <th>Image</th> -->
                        <th colspan="3" class="text-center">Jenis Kelamin</th>
                        <th rowspan="2">Action</th>
                        
                    
                    </tr>
                    <tr>
                        <th>Laki-Laki</th>
                        <th>Perempuan</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $data = mysqli_query($link,"SELECT * FROM tm_gender INNER JOIN unit_kerja ON unit_kerja.id_unit = tm_gender.id_unit");
                    while($row = mysqli_fetch_array($data)) {
                        $tahun = $row['tahun'];
                        if($tahun == '1'){
                            $dataTahun = '2015';
                        }
                        else if($tahun == '2'){
                            $dataTahun = '2016';
                        }
                        else if($tahun == '3'){
                            $dataTahun = '2017';
                        }
                        else if($tahun == '4'){
                            $dataTahun = '2018';
                        }
                        else if($tahun == '5'){
                            $dataTahun = '2019';
                        }
                        else if($tahun == '6'){
                            $dataTahun = '2020';
                        }
                        else if($tahun == '7'){
                            $dataTahun = '2021';
                        }
                        else if($tahun == '8'){
                            $dataTahun = '2022';
                        }
                        else if($tahun == '9'){
                            $dataTahun = '2023';
                        }

                ?>
                    <tr>
                        <td><?php echo $dataTahun ?></td>
                        <td><?php echo $row['nama_unit'] ?></td>
                        <td><?php echo $row['male'] ?></td>
                        <td><?php echo $row['female'] ?></td>
                        <td><?php echo $row['total'] ?></td>
                        <td>
                            <a class="btn btn-primary btn-outline" href="?page=editGender&id=<?php echo $row['id_gender'] ?>"">Edit</a>
                            <a class="btn btn-danger btn-outline" href="?page=deleteGender&id=<?php echo $row['id_gender'] ?>">Delete</a>
                        </td>
                        
                    </tr>
                    <?php 
                    }
                    ?>
                </tbody>
        </table>
    </div>
    
    
</div>  

<?php 
$homeGender = ob_get_clean();
?>