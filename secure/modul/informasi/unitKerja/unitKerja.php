<?php 
ob_start();
if(isset($_POST['kirim'])){
    $nama = mysqli_real_escape_string($link,$_POST['nama_unit']);

    
    $query = mysqli_query($link,"INSERT INTO unit_kerja (nama_unit) 
                VALUES ('$nama')");
    if($query){
        echo 'sukses';
    }
    else {
        echo 'gagal';
    }

}
?>
<button id="addPhoto" class="btn btn-primary dim" type="button" data-toggle="modal" data-target="#myModal">
    <i class="fa fa-plus"></i> Data Unit Kerja
</button>   
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!-- <i class="fa fa-pencil modal-icon"></i> -->
                <h4 class="modal-title"></h4>
                <h3 class="font-bold">
                    Form Unit Kerja
                </h3>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
                    
                   
                    <div class="form-group">
                        <label class="control-label">Unit Kerja</label>
                        <input type="text" class="form-control" name="nama_unit" required/>
                    </div>
                    
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" id="kirim" name="kirim" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <table id="unitkerja" class="display" style="width:100%">
            <thead>
                    <tr>
                        
                        <th>Nama Unit Kerja</th>
                        <!-- <th>Image</th> -->
                        <th>Action</th>
                    
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $data = mysqli_query($link,"SELECT * FROM unit_kerja");
                    while($row = mysqli_fetch_array($data)) {
                        

                ?>
                    <tr>
                        <td><?php echo $row['nama_unit'] ?></td>
                        
                        <td>
                            <a class="btn btn-primary btn-outline" href="?page=editUnit&id=<?php echo $row['id_unit'] ?>"">Edit</a>
                            <a class="btn btn-danger btn-outline" href="?page=deleteUnit&id=<?php echo $row['id_unit'] ?>">Delete</a>
                        </td>
                        
                    </tr>
                    <?php 
                    }
                    ?>
                </tbody>
        </table>
    </div>
    
    
</div>  
<?php 
$unitKerja = ob_get_clean();
?>