<?php 
ob_start();
$id = $_GET['id'];
if(isset($_POST['update'])){

    $tanggal = mysqli_real_escape_string($link,$_POST['tgl_laporan']);
    $nama = mysqli_real_escape_string($link,$_POST['nama']);
    $alamat = mysqli_real_escape_string($link,$_POST['alamat']);
    $gender = mysqli_real_escape_string($link,$_POST['gender']);
    $usia = mysqli_real_escape_string($link,$_POST['usia']);
    $pendidikan = mysqli_real_escape_string($link,$_POST['pendidikan']);
    $kekerasan = mysqli_real_escape_string($link,$_POST['kekerasan']);
    $relawan = mysqli_real_escape_string($link,$_POST['relawan']);

    $queryUpdateText = mysqli_query($link,"UPDATE tm_kasus SET tgl_laporan='$tanggal',
                                        nama='$nama',
                                        alamat='$alamat',
                                        gender='$gender',
                                        usia='$usia',
                                        pendidikan='$pendidikan',
                                        kekerasan='$kekerasan',
                                        relawan='$relawan'
                                        WHERE tm_kasus.id_kasus ='$id'");
    if($queryUpdateText){
        echo 'sukses';
    }
    else {
        echo 'gagal';
    }
    

}
else {
    $data = mysqli_query($link,"SELECT * FROM tm_kasus WHERE tm_kasus.id_kasus='$id'");
    $row = mysqli_fetch_array($data);

?>
<h1 class="text-center">Form Edit Laporan</h1>
<form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label class="control-label">Tanggal Laporan</label>
            <input type="date" class="form-control" name="tgl_laporan" value="<?php echo $row['tgl_laporan'] ?>"/>
        </div>
        <div class="form-group">
            <label class="control-label">Nama Lengkap</label>
            <input type="text" class="form-control" name="nama" value="<?php echo $row['nama'] ?>"/>
        </div>
        <div class="form-group">
            <label class="control-label">Alamat</label>
            <textarea cols="5" rows="5" class="form-control" name="alamat"><?php echo $row['alamat'] ?></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">Jenis Kelamin</label>
            <select class="form-control" name="gender" >
                <option value="1" <?php if($row['gender'] == "1"){ echo "selected";} ?>>Laki-laki</option>
                <option value="2" <?php if($row['gender'] == "2"){ echo "selected";} ?>>Perempuan</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Usia</label>
            <input type="text" class="form-control" name="usia" value="<?php echo $row['usia'] ?>"/>
        </div>
        <div class="form-group">
            <label class="control-label">Pendidikan</label>
            <input type="text" class="form-control" name="pendidikan"  value="<?php echo $row['pendidikan'] ?>"/>
        </div>
        <div class="form-group">
            <label class="control-label">Jenis Kekerasan</label>
            <select class="form-control" name="kekerasan" >
                <option value="1" <?php if($row['kekerasan'] == "1"){ echo "selected";} ?>>KTP</option>
                <option value="2" <?php if($row['kekerasan'] == "2"){ echo "selected";} ?>>KDRT</option>
                <option value="3" <?php if($row['kekerasan'] == "3"){ echo "selected";} ?>>KTA</option>
                <option value="4" <?php if($row['kekerasan'] == "4"){ echo "selected";} ?>>Korban Trafficking</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Relawan yang menangani</label>
            <input type="text" class="form-control" name="relawan" value="<?php echo $row['relawan'] ?>"/>
        </div>
        
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-warning" id="kirim" name="update" value="Update">
        </div>
</form>
<?php 
}
$editKasus = ob_get_clean();
?>