<?php 
ob_start();

if(isset($_POST['kirim'])){
    $tanggal = mysqli_real_escape_string($link,$_POST['tgl_laporan']);
    $nama = mysqli_real_escape_string($link,$_POST['nama']);
    $alamat = mysqli_real_escape_string($link,$_POST['alamat']);
    $gender = mysqli_real_escape_string($link,$_POST['gender']);
    $usia = mysqli_real_escape_string($link,$_POST['usia']);
    $pendidikan = mysqli_real_escape_string($link,$_POST['pendidikan']);
    $kekerasan = mysqli_real_escape_string($link,$_POST['kekerasan']);
    $relawan = mysqli_real_escape_string($link,$_POST['relawan']);
    
    $query = mysqli_query($link,"INSERT INTO tm_kasus (tgl_laporan,nama,alamat,gender,usia,pendidikan,kekerasan,relawan) 
                VALUES ('$tanggal','$nama','$alamat','$gender','$usia','$pendidikan','$kekerasan','$relawan')");
    if($query){
        echo 'sukses';
    }
    else {
        echo 'gagal';
    }

}

/**
 * Query Grafik
 */
$grafikQuery = mysqli_query($link,"SELECT kekerasan,count(tm_kasus.kekerasan) as 'jumlah' FROM tm_kasus group by tm_kasus.kekerasan");


$usiaArray = array();
$namaArray = array();
while ($row = mysqli_fetch_assoc($grafikQuery)) {
    $kekerasan = $row['kekerasan'];
    if($kekerasan == '1' ){
        $dataKekerasan = 'KTP';
    }
    else if($kekerasan == '2'){
        $dataKekerasan = 'KDRT';
    }
    else if($kekerasan == '3'){
        $dataKekerasan = 'KTA';
    }
    else if($kekerasan == '4'){
        $dataKekerasan = 'KORBAN TRAFFICKING';
    }

    $arrayPie[] =  "["."'".$dataKekerasan."'".",".intval($row['jumlah'])."]";
}
?>
<button id="addPhoto" class="btn btn-primary dim" type="button" data-toggle="modal" data-target="#myModal">
    <i class="fa fa-plus"></i> Data Kasus Kekerasan
</button>   
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!-- <i class="fa fa-pencil modal-icon"></i> -->
                <h4 class="modal-title"></h4>
                <h3 class="font-bold">
                    Form Kasus Kekerasan
                </h3>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label">Tanggal Laporan</label>
                        <input type="date" class="form-control" name="tgl_laporan" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nama Lengkap</label>
                        <input type="text" class="form-control" name="nama" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Alamat</label>
                        <textarea cols="5" rows="5" class="form-control" name="alamat" required></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Jenis Kelamin</label>
                        <select class="form-control" name="gender" required>
                            <option value="1">Laki-laki</option>
                            <option value="2">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Usia</label>
                        <input type="text" class="form-control" name="usia" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Pendidikan</label>
                        <input type="text" class="form-control" name="pendidikan" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Jenis Kekerasan</label>
                        <select class="form-control" name="kekerasan" required>
                            <option value="1">KTP</option>
                            <option value="2">KDRT</option>
                            <option value="3">KTA</option>
                            <option value="4">Korban Trafficking</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Relawan yang menangani</label>
                        <input type="text" class="form-control" name="relawan" required/>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" id="kirim" name="kirim" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-7">
        <table id="example" class="display" style="width:100%">
            <thead>
                    <tr>
                        <th>Tanggal Laporan</th>
                        <th>Nama</th>
                        <!-- <th>Image</th> -->
                        <th>Action</th>
                    
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $data = mysqli_query($link,"SELECT * FROM tm_kasus");
                    while($row = mysqli_fetch_array($data)) {
                        

                ?>
                    <tr>
                        <td><?php echo $row['tgl_laporan'] ?></td>
                        <td><?php echo $row['nama'] ?></td>
                        <!-- <td><img src="fileSecret/<?php echo $row['files'] ?>"</td> -->
                        <td>
                            <a class="btn btn-primary btn-outline" href="?page=editKasus&id=<?php echo $row['id_kasus'] ?>"">Edit</a>
                            <a class="btn btn-danger btn-outline" href="?page=deleteKasus&id=<?php echo $row['id_kasus'] ?>">Delete</a>
                        </td>
                        
                    </tr>
                    <?php 
                    }
                    ?>
                </tbody>
        </table>
    </div>
    <div class="col-md-5">
        <h3>Grafik </h3>
        <div id="container" style="width:100%; height: 400px; margin: 0 auto"></div>
    </div>
    
</div>  
<?php 
$homeKasus = ob_get_clean();
?>