<?php 

    switch($_GET['page']){

        /**
         * Dashboard
         */
        case 'home':
            if ($_GET['page']){
                include './modul/home.php';
                $content = $home;
            }
            break;
        /**
         * Data
         */
        case 'kasus':
            if ($_GET['page']){
                include './modul/data/home.php';
                $content = $homeKasus;
            }
            break;
        
        case 'deleteKasus':
            if ($_GET['page']){
                include './modul/data/delete.php';
                $content = $deleteKasus;
            }
            break;
        
        case 'editKasus':
            if ($_GET['page']){
                include './modul/data/edit.php';
                $content = $editKasus;
            }
            break;
        case 'laporan':
            if ($_GET['page']){
                include './modul/data/laporan.php';
                $content = $laporan;
            }
            break;
        
        /**
         * Publikasi
         */

        case 'photo':
            if ($_GET['page']){
                include './modul/publikasi/foto/home.php';
                $content = $photo;
            }
            break;
        case 'deletePhoto':
            if ($_GET['page']){
                include './modul/publikasi/foto/delete.php';
                $content = $deletePhoto;
            }
            break;

        case 'editPhoto':
            if ($_GET['page']){
                include './modul/publikasi/foto/edit.php';
                $content = $editPhoto;
            }
            break;
            
        case 'video':
            if ($_GET['page']){
                include './modul/publikasi/video/home.php';
                $content = $video;
            }
            break;
        case 'deleteVideo':
            if ($_GET['page']){
                include './modul/publikasi/video/delete.php';
                $content = $deleteVideo;
            }
            break;
        case 'viewVideo':
            if ($_GET['page']){
                include './modul/publikasi/video/view.php';
                $content = $viewVideo;
            }
            break;
        case 'editVideo':
            if ($_GET['page']){
                include './modul/publikasi/video/edit.php';
                $content = $editVideo;
            }
            break;

        case 'artikel':
            if ($_GET['page']){
                include './modul/publikasi/artikel/home.php';
                $content = $artikel;
            }
            break;
        case 'editArtikel':
            if ($_GET['page']){
                include './modul/publikasi/artikel/edit.php';
                $content = $editArtikel;
            }
            break;
        case 'deleteArtikel':
            if ($_GET['page']){
                include './modul/publikasi/artikel/delete.php';
                $content = $deleteArtikel;
            }
            break;

        /**
         * Infromasi
         */
        case 'gender':
            if ($_GET['page']){
                include './modul/informasi/gender/home.php';
                $content = $homeGender;
            }
            break;
        case 'deleteGender':
            if ($_GET['page']){
                include './modul/informasi/gender/delete.php';
                $content = $deleteGender;
            }
            break;
        case 'editGender':
            if ($_GET['page']){
                include './modul/informasi/gender/editGender.php';
                $content = $editGender;
            }
            break;
        case 'unitKerja':
            if ($_GET['page']){
                include './modul/informasi/unitKerja/unitKerja.php';
                $content = $unitKerja;
            }
            break;
        case 'editUnit':
            if ($_GET['page']){
                include './modul/informasi/unitKerja/editUnit.php';
                $content = $editUnit;
            }
            break;
        case 'deleteUnit':
            if ($_GET['page']){
                include './modul/informasi/unitKerja/deleteUnit.php';
                $content = $deleteUnit;
            }
            break;


         /**
         * Pengaduan
         */
        case 'pengaduan':
            if ($_GET['page']){
                include './modul/pengaduan/home.php';
                $content = $homePengaduan;
            }
            break;
        case 'editStatus':
            if ($_GET['page']){
                include './modul/pengaduan/detail.php';
                $content = $detailPengaduan;
            }
            break;
            

        default :
            include 'modul/home.php';
            $content = $home;
            break;
    }

?>