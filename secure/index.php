<?php 
session_start();
error_reporting('E_ALL');
include 'lib/db.php';
if ($_SESSION){
	header('location:page.php');
}
if (isset($_POST['masuk'])){
	// $username = preg_replace('/[^A-Za-z0-9\  ]/', '', $_POST['username']);
	// $password = preg_replace('/[^A-Za-z0-9\  ]/', '', sha1($_POST['password']));
	

	// $sql = "select * from tm_user where username = '$username' AND password = '$password' ";
	// $result = mysqli_query($link,$sql);

	// if ($row = mysqli_fetch_assoc($result)){
		
		
	// 		$_SESSION['user'] = $row['id_user'];
	// 		$_SESSION['nama'] = $row['nama'];
	// 		$_SESSION['username'] = $username;
	// 		$_SESSION['password'] = $password;
		
	// 		header('location:page.php');
	// 	}
	// else {
	// 	echo "<script>alert('gagal')</script>";
    // }
    
    $username = mysqli_real_escape_string($link,$_POST['username']);
    $password = mysqli_real_escape_string($link,$_POST['password']);
    $queryLogin = mysqli_query($link,"SELECT * FROM tm_user WHERE username = '$username' ");
    $row = mysqli_fetch_assoc($queryLogin);

    if($queryLogin){
        $pass = $row['password'];
        if(password_verify($password,$pass)){
            
            
            $_SESSION['username'] = $username;
			$_SESSION['password'] = $password;
            header('location:page.php');
            // echo '<script>alert("sukses")</script>';
        }
    }
    else {
        echo '<script>alert("gagal")</script>';
    }


}

?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Login</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    


    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    
</head>

<body style="background-color:rgb(25, 230, 25);">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">
                    <img src="./img/logo-login.png" class="img-fluid"/>
                </h1>

            </div>
            <h3 style="color:#fff;">Login SIGADIS</h3>
            
            <form class="m-t" role="form" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" required="required" name="username">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="required" name="password">
                </div>
                <button type="submit" class="btn  block full-width m-b" style="background-color:#ff0000;color:#fff;" name="masuk">Login</button>

                
            </form>
            
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

</body>

</html>
