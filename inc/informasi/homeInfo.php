<?php
ob_start();
?>


<section class="g-mt-110">
    <!-- <h1 class="text-center animated rubberBand g-font-weight-700">INFORMASI</h1> -->
    <img class="img-fluid animated rubberBand" src="img/informasi/logo.png" />
    <div class="row no-gutters g-mb-30 animated rubberBand g-mt-50">
            
            <!-- Icon Blocks -->
            <div class="col-lg-4 g-bg-pink g-color-white text-center g-pa-60">
                <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                    <i class="fas fa-users"></i>     
                </span>
                <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Informasi Gender</h3>
                <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
                <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-pink--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="#">
                <i class="fas fa-arrow-circle-right"></i> Selengkapnya
                </a>
            </div>
            <!-- End Icon Blocks -->
            <!-- Icon Blocks -->
            <div class="col-lg-4 g-bg-lightred g-color-white text-center g-pa-60">
                <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                    <i class="fas fa-clipboard-list"></i>
                </span>
                <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Daftar Organisasi Perempuan</h3>
                <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
                <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-lightred--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="?page=kie">
                <i class="fas fa-arrow-circle-right"></i> Selengkapnya
                </a>
            </div>
            <!-- End Icon Blocks -->

            

            <!-- Icon Blocks -->
            <div class="col-lg-4 g-bg-red g-color-white text-center g-pa-60">
                <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                    <i class="fas fa-file-pdf"></i>
                </span>
                <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Daftar Peraturan</h3>
                <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
                <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-red--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="?page=homeInfo">
                <i class="fas fa-arrow-circle-right"></i> Selengkapnya
                </a>
            </div>

            
            <!-- End Icon Blocks -->
    </div>
</section>

<?php
$homeInfo = ob_get_clean();
?>
