<?php 
ob_start();
?>
<style>
    li#pagination {
        margin : 10px;
        /* background-color:red; */
        width:20px;
        font-size:20px;
        text-align:center;
        border:1px solid #444;
        
    }
    li#pagination a:hover {
        /* background-color:#444; */
        color:#444;
        list-style-type:none;
        
    }
</style>
<div class="container-fluid g-pos-rel g-z-index-1 g-pt-100 g-pb-70"  style=" background: url(img/pattern2.png)">
    <section class=" g-mt-100">
    <h1 class="text-center">Galeri Foto</h1>
        <div class="row text-center">
       
            <?php 

                    $dataperpage = 6;
                                
                    if (isset($_GET['halaman']))
                    {
                        $nopage = $_GET['halaman'];

                    }
                    else {
                        $nopage = 1;
                    }
                    $offset = ($nopage-1)*$dataperpage;
                    $data = mysqli_query($link,"SELECT * FROM tm_foto ORDER BY tm_foto.tgl_upload DESC limit $offset,$dataperpage");
                    if(mysqli_num_rows($data) > 0){
                    while($row = mysqli_fetch_array($data)){
                        $postID = $row['id_foto'];
                ?>
               
                    <div class="col-md-4 g-mb-30">
                        <div class="u-shadow-v21 g-bg-white rounded g-px-20 g-py-30" id="postdata">
                            <img src="secure/fileSecret/<?php echo $row['files'] ?>"  class="img-fluid"/>
                        </div>
                        
                    </div>
                
                <?php 
                    }
                }
                else {
                    echo 'belum ada data';
                }
                ?>
               
            
        </div>

        <div class="row text-center" style="margin-top: 5%; ">
                
				<?php
					//mencari jumlah semua data 
					$query = "select COUNT(*) as jumdata from tm_foto";
					$hasil = mysqli_query($link, $query);
					$data = mysqli_fetch_array($hasil);
					
					$jumdata = $data['jumdata'];
					
					$jumpage = ceil($jumdata/$dataperpage);
					
					// echo '<nav class="text-center" aria-label="Page Navigation">';
                    echo '<ul class="pagination">';
                    
                    if ($nopage > 1) { 
                        // echo  "<li class='list-inline-item g-hidden-sm-down' >
                        // <a class='u-pagination-v1__item u-pagination-v1-1 g-rounded-50 g-pa-12-19' href=?page=galeri&halaman=".($nopage-1)." aria-label=\"Previous\"><span aria-hidden=\"true\">&laquo;</span></a> </li>";
                        echo  "<li id=\"pagination\">
                        <a href=?page=galeri&halaman=".($nopage-1)." aria-label=\"Previous\"><span aria-hidden=\"true\">&laquo;</span></a> </li>";
                    } 
					
					for($page = 1; $page <= $jumpage; $page++)
					{
						if ((($page >= $nopage - 3) && ($page <= $nopage + 3)) || ($page == 1) || ($page == $jumpage))
						{
							if (($showPage == 1) && ($page != 2)) ;
							if (($showPage != ($jumpage - 1)) && ($page == $jumpage))  ;
							
                            // else echo " <li class='list-inline-item g-hidden-sm-down'> <a class='u-pagination-v1__item u-pagination-v1-1 g-rounded-50 g-pa-12-19' href=?page=galeri&halaman=".$page."><b>".$page."</b></a></li> ";
                            else echo " <li id=\"pagination\"> <a href=?page=galeri&halaman=".$page."><b>".$page."</b></a></li> ";
							$showPage = $page;
						}
					}
					if ($nopage < $jumpage){
                    //     echo "
                    // <li class='list-inline-item g-hidden-sm-down'><a class='u-pagination-v1__item u-pagination-v1-1 g-rounded-50 g-pa-12-19' href=?page=galeri&halaman=".($nopage+1)." aria-label=\"Next\"> <span aria-hidden=\"true\">&raquo;</span></a></li>";
                    echo "
    				<li id=\"pagination\"><a  href=?page=galeri&halaman=".($nopage+1)." aria-label=\"Next\"> <span aria-hidden=\"true\">&raquo;</span></a></li>";
                    echo "</ul>";
                    // echo "</nav>";    
                    } 
					?>
		</div>

    </section>
</div>

<?php 
$galeri = ob_get_clean();
?>