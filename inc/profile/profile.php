<?php 
ob_start();
?>


<div class="container">
    <section class="g-mt-120">
        <div class="row">
            <div class="col-md-6">
                <div class="u-shadow-v3 g-mb-30 animated fadeInLeftBig">
                    <img src="img/kadis.jpg" alt="Image Description" class="img-fluid rounded w-100" style="height:400px;">
                </div>
            </div>

            <div class="col-md-6">
                <div class="u-shadow-v1-3 g-line-height-2 g-pa-40 g-mb-30 animated fadeInRightBig" role="alert" style="background-color:rgb(255, 51, 153);color:#fff;">
                <h3 class="h2 g-font-weight-600 g-mb-20 text-center">Kata Sambutan</h3>
                <p class="mb-0 text-justify g-font-weight-700 g-font-size-16">
                Puji syukur kehadirat Allah SWT, atas terbitnya Website SIGADIS. Semoga kehadirannya mampu menggambarkan Data Dan Informasi Gender  serta Pengaduan  kekerasan terhadap perempuan baik berupa kekerasan terhadap perempuan, trafficking serta Kekerasan Dalam rumah Tangga (KDRT) di  Kabupaten Bogor.  Kami menyadari keterbatasan dan kekurangan  dalam pembuatan dan pengelolaan Website ini sehingga saran dan masukan dari pihak – pihak masih diperlukan demi kesempurnaan di waktu yang akan datang. Demikian dan terima kasih, wassalamu’alaikum Wr. Wb. – Salam Sejahtera buat kita semua.
                </p>
                </div>
            </div>
        </div>
    </section>
    <section class="g-mt-30">
        <div class="u-shadow-v1-5 g-bg-pink g-color-white g-line-height-2 g-pa-40 g-mb-30 animated fadeInRight" role="alert">
            <div class="justify-content-center text-center ">
                <h2 class="text-uppercase">Sejarah</h2>
                <div class="d-inline-block g-width-35 g-height-2 g-bg-blue mb-2"></div>
            </div>
            <p class="mb-0" style="font-size:22px;">
            Dinas  pemberdayaan  Perempuan  dan Perlindungan Anak, Pengendalian  Peduduk  dan Keluarga  Berencana  ( BPPKB ) Kabupaten Bogor dibentuk berdasarkan Peraturan Daerah Kabupaten Bogor Nomor 12 Tahun 2008, tentang Pembentukan Lembaga Teknis Daerah. BPPKB mempunyai 2 (dua) Urusan Wajib yaitu Urusan Pemberdayaan Perempuan dan Perlindungan Anak ( PP PA ) dan Urusan Keluarga Berencana dan Keluarga Sejahtera ( KB KS ). Demi terarahnya pelaksanaan Tugas Pokok dan Fungsi Badan yang diwujudkan dalam pelaksanaan Program dan Kegiatan, memiliki Tujuan, Sasaran dan Landasan Kerja, yaitu
            <br>  Tujuan : 1. Terkendalinya Laju Pertumbuhan Penduduk Alami 2. Meningkatnya Tahapan Keluarga Sejahtera 3. Meningkatnya Pembinaan untuk kesejahteraan dan perlindungan anak 4. Terwujudnya Kesejahteraan dan perlindungan perempuan 5.  Meningkatnya kualitas dan akuntabilitas kinerja Sasaran : 1. Persentase partisipasi perempuan di lembaga pemerintah 2. Partisipasi perempuan di lembaga swasta 3. Rasio KDRT 4. Persentase jumlah tenaga kerja di bawah umur 5. Partisipasi angkatan kerja perempuan 6. Penyelesaian pengaduan perlindungan perempuan dan anak dari tindak kekerasan 7. Peningkatan pemahaman masyarakat tentang perlindungan dan pemenuhan hak-hak anak 8. Terbentuknya kecamatan ramah anak 9. Rata-rata jumlah anak per keluarga 10. Rasio akseptor KB Aktif 11. Cakupan peserta KB Aktif 12. Keluarga Pra sejahtera dan Keluarga sejahtera I 13. Cakupan pelayanan KB Gratis bagi keluarga Pra S dan KS I 14. Jumlah keluarga yang memiliki balita aktif dalam kelompok BKB 15. Jumlah keluarga yang memiliki remaja aktif dalamn kelompok BKR 16. Jumlah kelompok Usaha Peningkatan Pendapatan Keluarga Sejahtera ( UPPKS ).
            <br> Landasan Kerja : 1. Undang-Undang Nomor 52 Tahun 2009, tentang Perkembangan Kependudukan dan Pembangunan Keluarga 2. Undang-Undang Nomor 28 Tahun 1999, tentang Penyelenggaraan Negara yang Bersih dan Bebas dari Korupsi, Kolusi dan Nepotisme 3. Undang-Undang Nomor 23 Tahun 2002, tentang Perlindungan Anak 4. Undang-Undang Nomor 23 Tahun 2004, tentang Penghapusan Kekerasan dalam Rumah Tangga 5. Undang-Undang Nomor 32 Tahun 2004, tentang Pemerintahan Daerah 6. Undang-Undang Nomor 33 Tahun 2004, tentang Perimbangan Keuangan antara Pemerintah Pusat dan Daerah 7. Instruksi Presiden Nomor 9 Tahun 2000, tentang Pengarusutamaan Gender dalam Pembangunan Nasional 8. Peraturan Pemerintah Nomor 21 Tahun 1994, tentang Penyelenggaraan Pembangunan Keluarga Sejahtera 9. Peraturan Pemerintah Nomor 27 Tahun 1994, tentang Pengelolaan Perkembangan Kependudukan 10. Peraturan Pemerintah Nomor 4 Tahun 2006, tentang Penyelenggaraan dan Kerjasama Pemulihan Korban KDRT 11. Paraturan Menteri Dalam Negeri Nomor 13 Tahun 2006, tentang Pedoman Pengelolaan Keuangan Daerah 12. Peraturan Menteri Dalam Negeri Nomor 15 Tahun 2008, tentang Pedoman Umum Pelaksanaan Pengarusutamaan Gender di daerah 13. Paraturan Menteri Negara Pemberdayaan Perempuan RI Nomor 01/Permen PP/VI/2007 trentang Forum Koordinasi Penyelenggaraan Kerjasama Pencegahan dan Pemulihan Korban KDRT 14. Peraturan Daerah Kabupaten Bogor Nomor 12 Tahun 2008, tentang Lembaga Teknis Daerah.
            </p>
        </div>
    </section>

    <section class="g-mt-30">
        <div class="row">
            <div class="col-md-6">
                 <div class="u-shadow-v1-5 g-bg-pink g-color-white g-line-height-2 g-pa-40 g-mb-30" role="alert">
                    <div class="justify-content-center text-center ">
                        <h2 class="text-uppercase">Visi dan Misi</h2>
                        <div class="d-inline-block g-width-35 g-height-2 g-bg-blue mb-2"></div>
                    </div>
                    <blockquote class="blockquote g-mb-10">
                        <p class="m-b-0" style="font-size:22px;">
                        <b>Visi</b> : " Terwujudnya Kesetaraan Gender , perlindungan Perempuan dan anak serta keluarga kecil sejahtera "
                        </p>
                    </blockquote>
                    <blockquote class="blockquote g-mb-10">
                        <p class="m-b-0" style="font-size:22px;">
                        <b>Misi</b> : " Meningkatkan kualitas hidup dan perlindungan perempuan Meningkatkan kesejahteraan dan perlindungan anak Meningkatkan pelayanan KB dan kesehatan reproduksi Meningkatkan Pembangunan keluarga Sejahtera "
                        </p>
                    </blockquote>
                    
                </div>
            </div>
            <div class="col-md-6">
                <div class="u-shadow-v1-5 g-bg-pink g-color-white g-line-height-2 g-pa-40 g-mb-30" role="alert">
                    <div class="justify-content-center text-center ">
                        <h2 class="text-uppercase">Tupoksi</h2>
                        <div class="d-inline-block g-width-35 g-height-2 g-bg-blue mb-2"></div>
                    </div>
                    <h3><b>TUGAS POKOK : </b></h3> <br>
                    <p class="mb-0" style="font-size:22px;">
                        Membantu Bupati dalam melaksanakan penyusunan dan pelaksanaan kebijakan daerah di bidang pemberdayaan perempuan dan keluarga berencana.Badan Pemberdayaan Perempuan dan Keluarga Berencana
                    </p>
                    <h3><b>Fungsi : </b></h3> <br>
                    <ul class="list-unstyled">
                        <li style="font-size:22px;" class="media g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-white-left g-rounded-3 g-pa-20 g-mb-7">
                        Perumusan kebijakan teknis dibidang Pemberdayaan Perempuan dan Keluarga Berencana.
                        </li> 
                        <li style="font-size:22px;" class="media g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-white-left g-rounded-3 g-pa-20 g-mb-7">Pemberian dukungan atas penyelenggaraan pemerintahan daerah dibidang Pemberdayaan Perempuan dan Keluarga Berencana.</li>
                        <li style="font-size:22px;" class="media g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-white-left g-rounded-3 g-pa-20 g-mb-7">Pembinaan dan pelaksanaan tugas di bidang Pemberdayaan Perempuan dan Keluarga Berencana.</li>
                        <li style="font-size:22px;" class="media g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-white-left g-rounded-3 g-pa-20 g-mb-7">Pelaksanaan tugas lain yang diberikan oleh Bupati sesuai dengan tugas dan fungsinya.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>



<?php 
$profile = ob_get_clean();
?>