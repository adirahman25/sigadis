<?php 
ob_start();

    
?>
<div class="container">
    <!-- <div class="row" style="margin-top:110px;"> -->
        <section  style="margin-top:110px;">
        <div class="text-center">
            <img src="secure/img/logo-login.png" alt="Image Description">
        </div>
        </section>
        <section> 
            <div class="g-color-black col-md-12">
                    <h2 class="text-center g-font-weight-700 g-font-size-30 text-uppercase">
                    Sistem
                    <span class="u-text-animation u-text-animation--typing"></span>
                    </h2>
            </div>
        </section>
    <!-- </div> -->
   
    
</div>



<div class="row no-gutters">
    <!-- Icon Blocks -->
    <div class="col-lg-3 g-bg-pink g-color-white text-center g-pa-60">
        <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                <i class="far fa-images"></i>
        </span>
        <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Galeri</h3>
        <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
        <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-pink--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="?page=galeri">
        <i class="fas fa-arrow-circle-right"></i> Selengkapnya
        </a>
    </div>
    <!-- End Icon Blocks -->

    <!-- Icon Blocks -->
    <div class="col-lg-3 g-bg-red g-color-white text-center g-pa-60">
        <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
            <i class="fab fa-youtube"></i>
        </span>
        <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Video</h3>
        <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
        <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-red--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="?page=video">
        <i class="fas fa-arrow-circle-right"></i> Selengkapnya
        </a>
    </div>
    <!-- End Icon Blocks -->


    <div class="col-lg-3 g-bg-primary g-color-white text-center g-pa-60">
        <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
            <i class="fas fa-info"></i>
        </span>
        <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Informasi</h3>
        <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
        <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-primary--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="?page=homeInfo">
        <i class="fas fa-arrow-circle-right"></i> Selengkapnya
        </a>
    </div>

    <div class="col-lg-3 g-bg-black g-color-white text-center g-pa-60">
        <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
            <i class="fas fa-lock"></i>
        </span>
        <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">USERS</h3>
        <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
        <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-black--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="http://localhost/web/users/" target="_blank">
        <i class="fas fa-arrow-circle-right"></i> Register / Sign In
        </a>
    </div>
    <!-- End Icon Blocks -->
</div>


<section class="justify-content-center text-center g-mt-30">
    <h2 class="text-uppercase">Berita Terbaru</h2>
    <div class="d-inline-block g-width-35 g-height-2 g-bg-pink mb-2"></div>
</section>

<!-- News Block -->
<section class="g-mt-30">
    <div id="promo-carousel" class="js-carousel" data-infinite="true" data-autoplay="true" data-lazy-load="ondemand" data-speed="7000" data-center-mode="true" data-center-padding="200px" data-slides-show="3" data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-width-45 g-height-45 g-font-size-20 g-brd-style-solid g-brd-2 g-color-white g-bg-black-opacity-0_5--hover g-transition-0_3 rounded-circle"
        data-arrow-left-classes="fa fa-angle-left g-left-20" data-arrow-right-classes="fa fa-angle-right g-right-20" style="background-color:#19e619;">
        <!-- Article -->
        <div class="js-slide g-py-10 g-px-5">
            <article class="u-block-hover">
                <figure class="g-bg-cover g-bg-white-gradient-opacity-v1--after">
                    <img class="img-fluid w-100 u-block-hover__main--mover-up" data-lazy="img/3.png" alt="Image description">
                </figure>

                 <span class="g-pos-abs g-top-20 g-left-20">
                    <a class="btn btn-sm u-btn-purple rounded-0" href="#">
                        <i class="icon-energy"></i>
                    </a>
                    <a class="btn btn-sm u-btn-black rounded-0" href="#">July 15, 2017</a>
                </span>

            <div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
                <h3 class="h3 g-font-weight-300 g-mb-20">
                    <a class="g-color-white g-color-white--hover" href="#">Top 10 bust be used iOS apps for your next project</a>
                </h3>

                <div class="media">
                <div class="d-flex mr-3">
                    <img class="g-width-30 g-height-30 g-rounded-50x" data-lazy="img/img2.jpg" alt="Image description">
                </div>
                <div class="media-body align-self-center g-color-white">
                    <p class="mb-0">Ashley Dolwood</p>
                </div>
                </div>
            </div>
            </article>
        </div>
        <!-- End Article -->

        <!-- Article -->
        <div class="js-slide g-py-10 g-px-5">
            <article class="u-block-hover">
                <figure class="g-bg-cover g-bg-white-gradient-opacity-v1--after">
                    <img class="img-fluid w-100 u-block-hover__main--mover-up" data-lazy="img/4.png" alt="Image description">
                </figure>

                 <span class="g-pos-abs g-top-20 g-left-20">
                    <a class="btn btn-sm u-btn-purple rounded-0" href="#">
                        <i class="icon-energy"></i>
                    </a>
                    <a class="btn btn-sm u-btn-black rounded-0" href="#">July 15, 2017</a>
                </span>

            <div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
                <h3 class="h3 g-font-weight-300 g-mb-20">
                    <a class="g-color-white g-color-white--hover" href="#">Top 10 bust be used iOS apps for your next project</a>
                </h3>

                <div class="media">
                <div class="d-flex mr-3">
                    <img class="g-width-30 g-height-30 g-rounded-50x" data-lazy="img/img2.jpg" alt="Image description">
                </div>
                <div class="media-body align-self-center g-color-white">
                    <p class="mb-0">Ashley Dolwood</p>
                </div>
                </div>
            </div>
            </article>
        </div>
        <!-- End Article -->

        <!-- Article -->
        <div class="js-slide g-py-10 g-px-5">
            <article class="u-block-hover">
                <figure class="g-bg-cover g-bg-white-gradient-opacity-v1--after">
                    <img class="img-fluid w-100 u-block-hover__main--mover-up" data-lazy="img/3.png" alt="Image description">
                </figure>

                 <span class="g-pos-abs g-top-20 g-left-20">
                    <a class="btn btn-sm u-btn-purple rounded-0" href="#">
                        <i class="icon-energy"></i>
                    </a>
                    <a class="btn btn-sm u-btn-black rounded-0" href="#">July 15, 2017</a>
                </span>

            <div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
                <h3 class="h3 g-font-weight-300 g-mb-20">
                    <a class="g-color-white g-color-white--hover" href="#">Top 10 bust be used iOS apps for your next project</a>
                </h3>

                <div class="media">
                <div class="d-flex mr-3">
                    <img class="g-width-30 g-height-30 g-rounded-50x" data-lazy="img/img2.jpg" alt="Image description">
                </div>
                <div class="media-body align-self-center g-color-white">
                    <p class="mb-0">Ashley Dolwood</p>
                </div>
                </div>
            </div>
            </article>
        </div>
        <!-- End Article -->

        <!-- Article -->
        <div class="js-slide g-py-10 g-px-5">
            <article class="u-block-hover">
                <figure class="g-bg-cover g-bg-white-gradient-opacity-v1--after">
                    <img class="img-fluid w-100 u-block-hover__main--mover-up" data-lazy="img/4.png" alt="Image description">
                </figure>

                 <span class="g-pos-abs g-top-20 g-left-20">
                    <a class="btn btn-sm u-btn-purple rounded-0" href="#">
                        <i class="icon-energy"></i>
                    </a>
                    <a class="btn btn-sm u-btn-black rounded-0" href="#">July 15, 2017</a>
                </span>

            <div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
                <h3 class="h3 g-font-weight-300 g-mb-20">
                    <a class="g-color-white g-color-white--hover" href="#">Top 10 bust be used iOS apps for your next project</a>
                </h3>

                <div class="media">
                <div class="d-flex mr-3">
                    <img class="g-width-30 g-height-30 g-rounded-50x" data-lazy="img/img2.jpg" alt="Image description">
                </div>
                <div class="media-body align-self-center g-color-white">
                    <p class="mb-0">Ashley Dolwood</p>
                </div>
                </div>
            </div>
            </article>
        </div>
        <!-- End Article -->

        

        <!-- Article -->
        <div class="js-slide g-py-10 g-px-5">
            <article class="u-block-hover">
                <figure class="g-bg-cover g-bg-white-gradient-opacity-v1--after">
                    <img class="img-fluid w-100 u-block-hover__main--mover-up" data-lazy="img/4.png" alt="Image description">
                </figure>

                 <span class="g-pos-abs g-top-20 g-left-20">
                    <a class="btn btn-sm u-btn-purple rounded-0" href="#">
                        <i class="icon-energy"></i>
                    </a>
                    <a class="btn btn-sm u-btn-black rounded-0" href="#">July 15, 2017</a>
                </span>

            <div class="g-pos-abs g-bottom-20 g-left-20 g-right-20">
                <h3 class="h3 g-font-weight-300 g-mb-20">
                    <a class="g-color-white g-color-white--hover" href="#">Top 10 bust be used iOS apps for your next project</a>
                </h3>

                <div class="media">
                <div class="d-flex mr-3">
                    <img class="g-width-30 g-height-30 g-rounded-50x" data-lazy="img/img2.jpg" alt="Image description">
                </div>
                <div class="media-body align-self-center g-color-white">
                    <p class="mb-0">Ashley Dolwood</p>
                </div>
                </div>
            </div>
            </article>
        </div>
        <!-- End Article -->
    </div>
</section>


<!-- block for video and photo-->


<!-- <div class="container">
    <section class="justify-content-center text-center g-mt-30">
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-uppercase">Foto Terbaru</h2>
                <div class="d-inline-block g-width-35 g-height-2 g-bg-blue mb-2"></div>
            </div>
            <div class="col-md-6">
                <h2 class="text-uppercase">Video Terbaru</h2>
                <div class="d-inline-block g-width-35 g-height-2 g-bg-blue mb-2"></div>
            </div>
        </div>
    </section>

    <section class="justify-content-center text-center g-mt-30">
        <div class="row">
            <div class="col-md-6">
                
                <div class="row">

                    <div class="col-md-3">
                        <div class="g-brd-around g-brd-gray-light-v4--hover">
                            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" data-fancybox-gallery="lightbox-gallery--17" href="img/img3.jpg" title="Lightbox Gallery" data-open-effect="bounceInDown" data-close-effect="bounceOutDown" data-open-speed="1000" data-close-speed="1000" data-blur-bg="true">
                                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="img/img3.jpg" alt="example-image">
                            </a>
                            
                        </div>  
                    </div>

                    <div class="col-md-3">
                        <div class="g-brd-around g-brd-gray-light-v4--hover">
                            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" data-fancybox-gallery="lightbox-gallery--17" href="img/img3.jpg" title="Lightbox Gallery" data-open-effect="bounceInDown" data-close-effect="bounceOutDown" data-open-speed="1000" data-close-speed="1000" data-blur-bg="true">
                                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="img/img3.jpg" alt="example-image">
                            </a>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="g-brd-around g-brd-gray-light-v4--hover">
                            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" data-fancybox-gallery="lightbox-gallery--17" href="img/img3.jpg" title="Lightbox Gallery" data-open-effect="bounceInDown" data-close-effect="bounceOutDown" data-open-speed="1000" data-close-speed="1000" data-blur-bg="true">
                                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="img/img3.jpg" alt="example-image">
                            </a>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="g-brd-around g-brd-gray-light-v4--hover">
                            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" data-fancybox-gallery="lightbox-gallery--17" href="img/img3.jpg" title="Lightbox Gallery" data-open-effect="bounceInDown" data-close-effect="bounceOutDown" data-open-speed="1000" data-close-speed="1000" data-blur-bg="true">
                                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="img/img3.jpg" alt="example-image">
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-3">
                        <div class="g-brd-around g-brd-gray-light-v4--hover">
                            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" data-fancybox-gallery="lightbox-gallery--17" href="img/img3.jpg" title="Lightbox Gallery" data-open-effect="bounceInDown" data-close-effect="bounceOutDown" data-open-speed="1000" data-close-speed="1000" data-blur-bg="true">
                                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="img/img3.jpg" alt="example-image">
                            </a>
                            
                        </div>  
                    </div>

                    <div class="col-md-3">
                        <div class="g-brd-around g-brd-gray-light-v4--hover">
                            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" data-fancybox-gallery="lightbox-gallery--17" href="img/img3.jpg" title="Lightbox Gallery" data-open-effect="bounceInDown" data-close-effect="bounceOutDown" data-open-speed="1000" data-close-speed="1000" data-blur-bg="true">
                                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="img/img3.jpg" alt="example-image">
                            </a>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="g-brd-around g-brd-gray-light-v4--hover">
                            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" data-fancybox-gallery="lightbox-gallery--17" href="img/img3.jpg" title="Lightbox Gallery" data-open-effect="bounceInDown" data-close-effect="bounceOutDown" data-open-speed="1000" data-close-speed="1000" data-blur-bg="true">
                                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="img/img3.jpg" alt="example-image">
                            </a>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="g-brd-around g-brd-gray-light-v4--hover">
                            <a class="js-fancybox d-block u-block-hover u-block-hover--scale-down" data-fancybox-gallery="lightbox-gallery--17" href="img/img3.jpg" title="Lightbox Gallery" data-open-effect="bounceInDown" data-close-effect="bounceOutDown" data-open-speed="1000" data-close-speed="1000" data-blur-bg="true">
                                <img class="img-fluid u-block-hover__main--grayscale u-block-hover__img" src="img/img3.jpg" alt="example-image">
                            </a>
                        </div>
                    </div>

                </div>
                    
            </div>
            <div class="col-md-6">
                <div id="carousel-08-1" class="js-carousel text-center g-mb-20" data-infinite="true" data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-width-35 g-height-40 g-font-size-18 g-color-gray g-bg-black g-mt-minus-10" data-arrow-left-classes="fa fa-angle-left g-left-0" data-arrow-right-classes="fa fa-angle-right g-right-0" data-nav-for="#carousel-08-2">
                    <div class="js-slide">
                        <div class="embed-responsive embed-responsive-16by9 g-mb-30">
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/JoxWfSgOTvU?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="js-slide">
                        <div class="embed-responsive embed-responsive-16by9 g-mb-30">
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/JoxWfSgOTvU?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div> -->

<!-- end block-->



 
<?php 
$home = ob_get_clean();
?>