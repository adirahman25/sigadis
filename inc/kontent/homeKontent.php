<?php 
ob_start();
?>

<section class="g-mt-110">
    <h1 class="text-center animated rubberBand g-font-weight-700">KONTENT</h1>
    <div class="row no-gutters g-mb-30 animated rubberBand g-mt-50">
            
            <!-- Icon Blocks -->
            <div class="col-lg-4 g-bg-indigo g-color-white text-center g-pa-60">
                <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                    <i class="fas fa-file-alt"></i>          
                </span>
                <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Kekerasan Terhadap Anak</h3>
                <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
                <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-indigo--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="#">
                <i class="fas fa-arrow-circle-right"></i> Selengkapnya
                </a>
            </div>
            <!-- End Icon Blocks -->
            <!-- Icon Blocks -->
            <div class="col-lg-4 g-bg-pink g-color-white text-center g-pa-60">
                <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                    <i class="fas fa-file-alt"></i>     
                </span>
                <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Kekerasan Terhadap Perempuan</h3>
                <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
                <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-pink--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="?page=kie">
                <i class="fas fa-arrow-circle-right"></i> Selengkapnya
                </a>
            </div>
            <!-- End Icon Blocks -->

            

            <!-- Icon Blocks -->
            <div class="col-lg-4 g-bg-red g-color-white text-center g-pa-60">
                <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                    <i class="fas fa-file-alt"></i>     
                </span>
                <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">KDRT</h3>
                <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
                <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-red--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="?page=homeInfo">
                <i class="fas fa-arrow-circle-right"></i> Selengkapnya
                </a>
            </div>

            
            <!-- End Icon Blocks -->
    </div>

    <div class="row no-gutters g-mb-30 animated rubberBand g-mt-50">
            
            <!-- Icon Blocks -->
            <div class="col-lg-4 g-bg-darkpurple g-color-white text-center g-pa-60">
                <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                    <i class="fas fa-file-alt"></i>          
                </span>
                <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Perdagangan Orang</h3>
                <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
                <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-darkpurple--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="#">
                <i class="fas fa-arrow-circle-right"></i> Selengkapnya
                </a>
            </div>
            <!-- End Icon Blocks -->
            <!-- Icon Blocks -->
            <div class="col-lg-4 g-bg-brown g-color-white text-center g-pa-60">
                <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                    <i class="fas fa-file-alt"></i>     
                </span>
                <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Psikologi</h3>
                <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
                <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-brown--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="?page=kie">
                <i class="fas fa-arrow-circle-right"></i> Selengkapnya
                </a>
            </div>
            <!-- End Icon Blocks -->

            

            <!-- Icon Blocks -->
            <div class="col-lg-4 g-bg-cyan g-color-white text-center g-pa-60">
                <span class="u-icon-v2 u-shadow-v24 g-brd-2 rounded-circle g-mb-25">
                    <i class="fas fa-file-alt"></i>     
                </span>
                <h3 class="h5 text-uppercase g-font-weight-600 g-mb-15">Statistik</h3>
                <!-- <p class="g-color-white-opacity-0_8 g-font-size-16 g-mb-20">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p> -->
                <a class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-cyan--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-12 g-px-20" href="?page=homeInfo">
                <i class="fas fa-arrow-circle-right"></i> Selengkapnya
                </a>
            </div>

            
            <!-- End Icon Blocks -->
    </div>
</section>

<?php
$homeKontent = ob_get_clean();
?>