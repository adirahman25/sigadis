<?php 

    switch($_GET['page']){
        
        /**
         * Menu main 
         */

        case 'home':
            if ($_GET['page']){
                include 'inc/home.php';
                $content = $home;
            }
            break;

        case 'profile':
            if ($_GET['page']){
                include 'inc/profile/profile.php';
                $content = $profile;
            }
            break;

        /**
         * Menu Informasi
         */
        case 'homeInfo':
            if ($_GET['page']){
                include 'inc/informasi/homeInfo.php';
                $content = $homeInfo;
            }
            break;
        case 'gender':
            if ($_GET['page']){
                include 'inc/informasi/gender.php';
                $content = $gender;
            }
            break;

        /**
         * Menu Galeri
         */
        case 'galeri':
            if ($_GET['page']){
                include 'inc/galeri/galeri.php';
                $content = $galeri;
            }
            break;

        /**
         * Menu Video
         */
        case 'video':
            if ($_GET['page']){
                include 'inc/video/home.php';
                $content = $video;
            }
            break;

        default :
            include 'inc/home.php';
            $content = $home;
            break;
    }

?>