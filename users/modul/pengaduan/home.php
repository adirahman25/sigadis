<?php 
ob_start();
$idUser = $_SESSION['id_user'];
?>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--12-col">
    <a href="?page=addPengaduan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="margin-bottom:30px;color:#fff;background-color:#ff0066;">
        <i class="material-icons">add</i> Pengaduan
    </a>
    <table class="mdl-data-table mdl-js-data-table  mdl-shadow--2dp" id="pengaduan">
        <thead>
            <tr>
            
                <th>Nama Pelapor</th>
                <th>Nama Korban</th>
                <th>Status</th> 
                <th>Acion</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $query = mysqli_query($link,"SELECT * FROM tm_pengaduan WHERE tm_pengaduan.id_login = '$idUser'");
            
            while($data = mysqli_fetch_array($query)){
                
                if($data['stat'] == "1"){
                    $status = "<span class=\"label label-danger\">Belum Diverifikasi</span>";
                }
                else if($data['stat'] == "2") {
                    $status = "<span class=\"label label-primary\">Sudah Diverifikasi</span>";
                }
        ?>
            <tr>
                <td><?php echo strtoupper($_SESSION['namaDepan'] . ' ' . $_SESSION['namaBelakang'])?></td>
                <td><?php echo strtoupper($data['nama_korban'])?></td>
                <td><?php echo $status; ?></td>
                <td>
                
                <a id="edit" href="?page=addPengaduan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="margin-bottom:30px;color:#fff;background-color:#ff0066;">
                    <i class="fas fa-edit"></i>
                </a>
                <!-- Multiline Tooltip -->
                <div class="mdl-tooltip" for="edit">
                    Edit Data 
                </div>
                <!-- <div class="mdl-tooltip" for="delete">
                    Delete Data 
                </div>
                <a id="delete" href="?page=addPengaduan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="margin-bottom:30px;color:#fff;background-color:#ff0066;">
                    <i class="fas fa-trash-alt"></i>
                </a> -->
                </td>
            </tr>
        <?php 
            }
        ?>
        </tbody>
    </table>
  </div>
</div>

<?php 
$homePengaduan = ob_get_clean();
?>