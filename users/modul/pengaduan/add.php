<?php 
ob_start();

if(isset($_POST['kirim'])){
    
    $idUser = $_SESSION['id_user'];
    // $namaPengadu = mysqli_real_escape_string($link,$_POST['nama_pengadu']);
    // $ktp = mysqli_real_escape_string($link,$_POST['ktp']);
    $alamat = mysqli_real_escape_string($link,$_POST['alamat']);
    $telp = mysqli_real_escape_string($link,$_POST['telp']);
    $statusPengadu = mysqli_real_escape_string($link,$_POST['status_pengadu']);
    $namaKorban = mysqli_real_escape_string($link,$_POST['nama_korban']);
    $jenisKelamin = mysqli_real_escape_string($link,$_POST['jenis_kelamin']);
    $usiaKorban = mysqli_real_escape_string($link,$_POST['usia_korban']);
    $files = mysqli_real_escape_string($link,$_POST['files']);
    $deskripsi = mysqli_real_escape_string($link,$_POST['deskripsi']);

    $query = mysqli_query($link,"INSERT INTO tm_pengaduan ( id_login,
                                                            alamat,
                                                            telp,
                                                            status_pengadu,
                                                            nama_korban,
                                                            jenis_kelamin,
                                                            usia_korban,
                                                            files,
                                                            deskripsi) VALUES 
                                                            ('$idUser','$alamat','$telp',
                                                            '$statusPengadu','$namaKorban','$jenisKelamin','$usiaKorban','$files','$deskripsi'
                                                            )");
    
    if($query){
        $message = [
            'title' => 'Berhasil!',
            'txt' => 'Pengaduan Terkirim, Akan Segera Kami Proses Terima Kasih',
            'type' => 'success'
        ];
    }else {
        $message = [
            'title' => 'Gagal!',
            'txt' => 'Gagal Buat Pengaduan, Silahkan Isi Form dengan Benar',
            'type' => 'error'
        ];
    }
}

?>
<div class="container">
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-primary" style="margin-top:30px;">
            
            <div class="panel-heading">Form Pengaduan</div>
            
            <div class="panel-body">
            <?php include 'message.php'; ?>
                <form method="post">
                    <div class="form-group">
                        <label>Nama Pelapor</label>
                        <input type="text" class="form-control" style="text-transform: uppercase;" value="<?php echo $_SESSION['namaDepan'] . ' ' . $_SESSION['namaBelakang'] ?>" required="required" readonly>
                    </div>
                    <div class="form-group">
                        <label>KTP</label><br/>
                        <input type="text" class="form-control"  maxlength="16" required="required" value="<?php echo $_SESSION['ktp'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Alamat Lengkap Pelapor</label>
                        <textarea class="form-control" name="alamat" cols="5" rows="5" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="number" class="form-control" name="telp" required="required">
                    </div>
                    <div class="form-group">
                        <label>Status Pelapor</label>
                        <select class=form-control name="status_pengadu">
                            <option value="1">-</option>
                            <option value="2">Korban</option>
                            <option value="3">Orang Tua</option>
                            <option value="4">Kakak / Adik</option>
                            <option value="5">Keluarga Dekat</option>
                            <option value="6">Tetangga</option>
                            <option value="7">Pendamping</option>
                            <option value="8">Orang Lain</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Korban</label>
                        <input type="text" class="form-control" style="text-transform: uppercase;" name="nama_korban" required="required">
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <select class=form-control name="jenis_kelamin">
                            <option value="1">Laki - Laki</option>
                            <option value="2">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Usia Korban</label>
                        <input type="number" class="form-control" name="usia_korban"  required="required">
                    </div>
                    <div class="form-group">
                        <label>Upload Gambar</label>
                        <input type="file" class="form-control" name="files" required="required">
                    </div>
                    <div class="form-group">
                        <label>Deskripsi Singkat</label>
                        <textarea class="form-control" name="deskripsi" cols="5" rows="5" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" name="kirim" value="Kirim">
                    </div>
                </form>
            </div>
        </div>
        
    </div>
</div>
</div>
<?php 
$addPengaduan = ob_get_clean();