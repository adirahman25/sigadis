<?php if (isset($message)): ?>
	<div class="message" data-message="<?= $message['txt'] ?>" data-type="<?= $message['type'] ?>" data-title="<?= $message['title'] ?>"></div>
<?php endif ?>