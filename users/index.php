<?php
session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include './lib/db.php'; 

//Load Composer's autoloader
require 'vendor/autoload.php';
require './lib/config-mail.php';

if(isset($_POST['register'])){

    $mail = new PHPMailer(true); 

    $nama_depan = mysqli_real_escape_string($link,$_POST['nama_depan']);
    $nama_belakang = mysqli_real_escape_string($link,$_POST['nama_belakang']);
    $ktp = mysqli_real_escape_string($link,$_POST['ktp']);
    $email = mysqli_real_escape_string($link,$_POST['email']);
    $username = mysqli_real_escape_string($link,$_POST['username']);
    $password = password_hash($_POST['password'],PASSWORD_DEFAULT) ;
    $user_key = uniqid();

    $queryCheck = "SELECT * FROM users_login WHERE username = '$username' OR email = '$email'";
	$checkResult = mysqli_query($link,$queryCheck);
	$row = mysqli_fetch_array($checkResult);
	if ($row) {
		$result = "Username / Email telah terpakai!";
	}

    if(empty($result)){
        $queryRegister = mysqli_query($link,"INSERT INTO users_login (nama_depan,nama_belakang,ktp,email,username,password,user_key) VALUES ('$nama_depan','$nama_belakang','$ktp','$email','$username','$password','$user_key')");
        
        $emailTo = $email;

	    $id = mysqli_insert_id($link);
	    $subject = '[SIGADIS] Activation Account For: '.$username;
	    $deskripsi = 'Link Activation: http://localhost/web/users/verify.php?id='.$id.'&user_key='.$user_key;
    
        //Server settings
	    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = SMTPUSER;                 // SMTP username
	    $mail->Password = SMTPPASS;                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    //Recipients
	    $mail->setFrom('no-reply@accounts.com', 'Activation Account');
	    $mail->addAddress($emailTo);     // Add a recipient
	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = $subject;
	    $mail->Body    = $deskripsi;

	    $mail->send();

		if ($queryRegister) {
			$result= "Berhasil, Silahkan check email anda!";
		}else{
			$result = "Gagal!";
		}
    
    }


   
}

if(isset($_POST['login'])){

    $username = mysqli_real_escape_string($link,$_POST['username']);
    $password = mysqli_real_escape_string($link,$_POST['password']);
    $queryLogin = mysqli_query($link,"SELECT * FROM users_login WHERE username = '$username' AND status = 1 ");
    $row = mysqli_fetch_assoc($queryLogin);

    if($queryLogin){
        $pass = $row['password'];
        if(password_verify($password,$pass)){
            
            $_SESSION['id_user'] = $row['id_login'];
            $_SESSION['namaDepan'] = $row['nama_depan'];
            $_SESSION['namaBelakang'] = $row['nama_belakang'];
            $_SESSION['ktp'] = $row['ktp'];
            $_SESSION['email'] = $row['email'];
            $_SESSION['username'] = $username;
			$_SESSION['password'] = $password;
            header('location:page.php');
            // echo '<script>alert("sukses")</script>';
        }
    }
    else {
        echo '<script>alert("gagal")</script>';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    

</head>
<body style="background-color:grey">
    
<div class="container" style="margin-top:30px;">
    <div class="row">
        <img class="img-fluid" src="./images/logo.png" alt="kabupaten bogor" style="margin: 0 auto;height:220px;"/>
    </div>
    <div class="row">
        <div class="col-md-6" style="background-color:rgb(24, 186, 155);padding-top:20px;padding-bottom:20px;">
            <h2 style="color:#fff;">Form Login</h2>
            <form method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                    <input name="username" type="text" class="form-control" placeholder="Username" required="required">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fas fa-lock"></i>
                        </span>
                    </div>
                    <input name="password" type="password" class="form-control" placeholder="Password" required="required">
                </div>
                <input type="submit" name="login" value="Login" class="btn btn-primary"/>
                <a href="forgot.php" class="btn btn-danger">Lupa Password</a>
            </form>
        </div>

        <div class="col-md-6" style="background-color:#fff;padding-top:20px;padding-bottom:20px;">
            <h2>Form Registrasi</h2>
            <form method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="far fa-address-card"></i>
                        </span>
                    </div>
                    <input name="nama_depan" type="text" class="form-control" placeholder="Nama Depan" required="required">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="far fa-address-card"></i>
                        </span>
                    </div>
                    <input name="nama_belakang" type="text" class="form-control" placeholder="Nama Belakang" required="required">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="far fa-address-card"></i>
                        </span>
                    </div>
                    <input name="ktp" type="text" class="form-control"  placeholder="Kartu Tanda Penduduk" maxlength="16" required="required">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="far fa-envelope"></i>
                        </span>
                    </div>
                    <input name="email" type="email" class="form-control" placeholder="Email" required="required">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                    <input name="username" type="text" class="form-control" placeholder="Username" required="required">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="fas fa-lock"></i>
                        </span>
                    </div>
                    <input name="password" type="password" class="form-control" placeholder="Password" required="required">
                </div>
                <input type="submit" name="register" value="Regsitrasi" class="btn btn-primary"/>
            </form>
        </div>
    </div>
    

</div>


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</body>
</html>